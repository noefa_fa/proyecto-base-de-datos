#  Proyecto Base de Datos - Unidad 3
**Librería Noefa**

La librería Noefa desea crear un sistema para gestionar los datos referentes a los libros que posee y sobre las ventas que se realizan.

Necesita almacenar los datos sobre los libros con el título del libro, editorial, genero, autor(es), el número de ejemplares y su respectivo precio; incluyendo un código que identifique cada libro. Además necesita información acerca de las compras que se realizan con el respectivo cliente y empleado que lo realiza. 

De esta manera se puede evaluar cuántos ejemplares hay y si se necesitan adquirir más, además de permitir establecer una clasificación por diversas características (valor, autor, editorial, etc).

Se propone crear una base de datos donde se almacene cada propiedad de lo que involucra las ventas de la librería con las siguiente características:

- Cada libro tiene solo un género que lo representa que será identificado por el su nombre, permitiendo una agrupación y búsqueda más eficiente.
- Disponer de una lista de autores que se encuentran en la librería, almacenando información referente al nombre, apellido o pseudonimo del autor correspondiente. Cabe destacar que existen libros publicados de manera anónima. 
- Generar una lista de editoriales que distribuyen los libros, donde una editorial distribuye un único libro. Cada libro se identifica por su género principal.
- Los empleados de la librería se almacenan dentro la base de datos para saber quien realiza las compras, donde se identifica por nombre, código y contacto. 
- En el caso de los clientes se almacenan cuando realicen compras, permitiendo que realicen numerosas compras si lo desean, guardando sus datos e identificándose por la librería con un código.
Se almacenan las compras hechas en la librería para mantener registros de ventas, incluyendo quién lo vende y quien compra dichos libros, además de la fecha de realización.


# Empezando
 
Se debe ejecutar la apliación por una terminar desde la localizacion de la carpeta correspondiente.
Una vez ejecutada la app, se mostrará una pestaña que permitira ingresar a la plataforma pero antes se debe ingresar la contraseña del administrador de la aplicación que sera: 

> contraseña: admin 

Una vez que entre el usuario obsevervara un menú principal con una serie de opciones de administrador. En la parte superior tendra una serie de botones los cuales serán los siguiente:
- Ayuda: Esté tendrá un botón que nos informara sobre la aplicación. 
- Vistas: Tendra dos opciones:

        1. view libros: Nos mostrará una tabla resumen de los libros que tiene cada autor relacionado. Se debe seleccionar un autor y ms¿se mostrará una tabla con los títulos de los libros, el género y la editorial.
        2. view empleado: Nos mostrará una tabla resumen de las ventas que tiene cada empleado. Se debe seleccionar un empleado y se mostrará una tabla con los títulos de los libros que ha vendido y en la fecha en que se realizó. 

- Gráficos: Tendra dos opciones:

        1. Histograma: Nos mostrará un histograma de la cantidad de libros que hay por género.  
        2. Scatter: Nos mostrará un gráfico de puntos de la ganancia que se obtiene por fecha 

- Triggers: Tendra dos opciones:

        1. Empleados llegada: Nos mostrará una tabla con todos los empleados que fueron añadidos recientemente con su respectiva fecha de ingreso. 
        2. Cambios precios: Nos mostrará una tabla con todos libros que tuvieron una reciente modificación en el precio, donde nos enseñara el id, título, precio y cambio de precio del respectivo libro. 

- Funciones: Tendra dos opciones:

        1. Descuenta: Se aplicara un descuento a todos los libros de un respectivo género. Para esto pedira el género y el respectivo descuento que desea aplicar. 
        2. Libros por editorial: Se mostrará la cantidad de libros que hay por cada editorial. Donde debe señalar de antemano la editorial que desea observar. 

- Dinámicas: Tendra dos opciones:

        1. Libros por precio: Se filtrará los libros por un precio mínimo y máximo qeu se debe indicar en las respectivas casillas. Luego en la opción de consultar se mostrará una tabla con el id, título y precio de los libros que cumplen con este requisito. 
        2. Libros venidos: Se filtrará los libros que han sido vendidos por un género determinado, el cual se debe señalar. Al consultar se mostrará una tabla con id, título y precio de los libros que han sido vendidos. 


También se mostrará una serie de opciones de administrador que se encontraran en el lado izquierdo de la pestaña que serán las siguientes:

- Libro: Se abrira una ventana donde se podra ingresar, modificar o eliminar libro. Donde contendra la información de id, título, precio, genero y editorial del respectivo libro. Es necesario que se haya ingresado con anterioridad género y editorial. 
- Autor: Se abrira una ventana donde se podra ingresar, modificar o eliminar un autor. Donde contendra la información de id, nombre, apellido o pseudonimo del respectivo autor. 
- Géneros: Se abrira una ventana donde se podra ingresar, modificar o eliminar un género. Donde contendra la información de id y nombre del respectivo género.
- Editorial: Se abrira una ventana donde se podra ingresar, modificar o eliminar un editorial. Donde contendra la información de id y nombre del respectivo editorial.
- Empleado: Se abrira una ventana donde se podra ingresar, modificar o eliminar un empleado. Donde contendra la información de id, nombre, apellido y télefono (que puede ser nulo) del respectivo empleado.
- Cliente: Se abrira una ventana donde se podra ingresar, modificar o eliminar un cliente. Donde contendra la información de id, nombre, apellido y télefono (que puede ser nulo) del respectivo cliente.
- Ejemplar: Se abrira una ventana donde se podra ingresar, modificar o eliminar un ejemplar. Donde contendra la información de id ejemplar, id libro y libro. Es necesario que se haya ingresado con anterioridad libro.
- Compra: Se abrira una ventana donde se podra ingresar, modificar o eliminar una compra. Donde contendra la información de id, fecha, total, cliente y empleado de dicha compra. Es necesario que se haya ingresado con anterioridad cliente y empleado. 
- Detalle venta: Se abrira una ventana donde se podra ingresar, modificar o eliminar detalle de venta. Donde contendra la información de id compra, id ejemplar, precio vendido y libro vendido. Es necesrio que se haya ingresado con anterioridad ejemplar y libro.
- Relación autor-libro: Se abrira una ventana donde se podra ingresar, modificar o eliminar relacion autor libro. Donde contendra información de id libro, id autor, libro y autor. Es necesario que se haya ingresado con anterioridad libro y autor. 

Al momento de no ejecutar el programa con un paramentro mostrara un error y recuerde que el archivo que desee analizar debe estar en la carpeta correspondiente del programa para poder reconocerlo, si no esta en la carpeta, mostrará un error. 

# Prerequisitos

- Sistema operativo linux: De preferencia Ubuntu o Debian

- Lenguaje de programación python

- Editor de texto

- Mysql:

Descargar (mysql-apt-config_0.8.15-1_all.deb) desde:

[https://dev.mysql.com/downloads/repo/apt/](https://dev.mysql.com/downloads/repo/apt/)

Luego instalar como usuario root desde terminal en carpeta de descargas:

`# dpkg -i mysql-apt-config_0.8.15-1_all.deb`

(activar las tres opciones: servidor, tools, otros. Pedirá clave para usuario root del servidor)

Actualizar lista de paquetes

`# apt update`

Instalar servidor 

`# apt install mysql-server`

Iniciar servidor:

`# systemctl start mysql.service`



- Instalación de librerías y tkinter:

`pip install mysql-connector-python`

`pip install matplotlib`

`pip install tkcalendar`

`sudo apt install python3-tk`

`sudo apt-get update`


# Ejecutando las pruebas

En la carpeta contenedora por medio de la terminal se debe ejecutar MySQL

`mysql -u root -p`

Primero se debe crear la base de datos:
- create database libreria;

Cargar el archivo dump:
- mysql -u root -p libreria < libreria.sql

Crear el usuario que permite la conexión a la aplicación:
- create user 'libreria'@'localhost' identified by 'libreria';

- grant all privileges on libreria.* to 'libreria'@'localhost';

- flush privileges;

Fuera de MySQL se debe ejecutar el programa por medio de la terminal en la carpeta que se encuntra el archivo:
`python3 libreria.py`


# Despliegue
La aplicación se debe ejecutar desde la terminal localizada en la carpeta que presenta los archivos de ésta. Se debe de ejecutar bajo el sistema operativo Linux.

# Construido con
- Ubuntu: Sistema operativo.
- Python: Es un lenguaje de programación
- Librerias: 
# Autores
Noemí Gómez - nogomez19@alumnos.utalca.cl
Josefa Hillmer - jhillmer19@alumnos.utalca.cl


