
import mysql.connector
from mysql.connector import errorcode
from tkinter import messagebox

# Comienzo a la clase de la base de datos
class Database:
    def __init__(self):
        self.db = None
        self.cursor = None
        # Intenta conectarse a la base de datos
        try:
            self.db = mysql.connector.connect(host="localhost",
                user="libreria", passwd="libreria", database="libreria")
            # Permitira la conexion con la base de datos
            self.cursor = self.db.cursor()
            print("Conectado exitosamente")

        # Si no puede, avisa
        except mysql.connector.Error as err:
            # Avisa del error generado
            texto = "No se pueden obtener los datos"
            messagebox.showerror(message = texto, title = "Error")
            print("No conectó a la base de datos")
            print(err)
            # Termina la aplicación
            exit()

    def run_sql_view(self, sql, params):
        try:
            # Ejecuta el select
            self.cursor.execute(sql, params)

        # Error
        except mysql.connector.Error as err:
            texto = "No se pueden obtener los datos"
            messagebox.showerror(message = texto, title = "Error")
            print("No se pueden obtener los datos")
            print(err)

    # Función que corre un select
    def run_select(self, sql):
        #sql se un string con un select en lenguaje sql
        try:
            # ejecuta
            self.cursor.execute(sql)
            # Guarda el resultado en result
            result = self.cursor.fetchall()
        # Si no resulta, avisa
        except mysql.connector.Error as err:
            texto = "No se pueden obtener los datos"
            messagebox.showerror(message = texto, title = "Error")
            print("No se pueden obtener los datos")
            print(err)
        # Retorna result
        return result

    # Función que corre una consulta select para un registro específico
    def run_select_filter(self, sql, params):
        # Excepción para ovtener los datos
        try:
            # Ejecuta
            self.cursor.execute(sql, params)
            result = self.cursor.fetchall()
        # Error
        except mysql.connector.Error as err:
            texto = "No se pueden obtener los datos"
            messagebox.showerror(message = texto, title = "Error")
            print("No se pueden obtener los datos")
            print(err)
        # Retorna resultado
        return result

    # Consulta de inserción, eliminación o modificación
    def run_sql(self, sql, params):
        try:
            # Ejecuta
            self.cursor.execute(sql, params)
            # Mensaje
            texto = "Se realizó la operación con éxito"
            messagebox.showinfo(message = texto, title = "Aviso")
            # Cambios
            self.db.commit()

        # Errores
        except mysql.connector.Error as err:
            #  No se elimina porque esta en otra tabla
            texto_error = "Error:"
            if err.errno == 1451:
                texto_error = texto_error + " Referencia en otra tabla"
            # Error en el tipo de dato
            elif err.errno == 1366:
                texto_error = texto_error + " Error en el tipo de dato: númericos enteros"
            # Ya existe un registro con ese ID
            elif err.errno == 1062:
                texto_error = texto_error + " Ya exite un registro con ese ID"
            # Error
            messagebox.showerror(message = texto_error, title = "Error")
            print("No se puede realizar la sql")
            print(err)
