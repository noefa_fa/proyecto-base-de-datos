import tkinter as tk
from tkinter import ttk
from tkinter import messagebox

class genero:
    def __init__(self, root, db):
        self.libreria = db
        self.data = []

        #Toplevel es una ventana que está un nivel arriba que la principal
        self.root = tk.Toplevel()
        self.root.geometry('750x400')
        self.root.title("Géneros")
        self.root.resizable(width=0, height=0)

        # toplevel modal
        self.root.transient(root)

        #
        self.__config_treeview_genero()
        self.__config_buttons_genero()

    def __config_treeview_genero(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1"))
        self.treeview.heading("#0", text = "Id")
        self.treeview.heading("#1", text = "Nombre")
    #    self.treeview.heading("#2", text = "")
        self.treeview.column("#0", minwidth = 200, width = 200, stretch = False)
        self.treeview.column("#1", minwidth = 450, width = 450, stretch = False)
    #    self.treeview.column("#2", minwidth = 50, width = 50, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 350, width = 750)
        self.llenar_treeview_genero()
        self.root.after(0, self.llenar_treeview_genero)

    def __config_buttons_genero(self):
        tk.Button(self.root, text="Insertar Género",
            command = self.insertar_genero).place(x = 0, y = 350, width = 250, height = 50)
        tk.Button(self.root, text="Modificar Género",
            command = self.modificar_genero).place(x = 250, y = 350, width = 250, height = 50)
        tk.Button(self.root, text="Eliminar Género",
            command = self.eliminar_genero).place(x = 500, y = 350, width = 250, height = 50)

    def llenar_treeview_genero(self):
        sql = "select * from Genero"
        # Ejecuta el select
        data = self.libreria.run_select(sql)

        # Si la data es distina a la que hay actualmente...
        if(data != self.data):
            # Elimina todos los rows del treeview
            self.treeview.delete(*self.treeview.get_children())#Elimina todos los rows del treeview
            for i in data:
                #Inserta los datos
                self.treeview.insert("", "end", text = i[0],
                    values = (i[1]), iid = i[0])
            self.data = data  #Actualiza la data


    def insertar_genero(self):
        insertar_genero(self.libreria, self)

    def modificar_genero(self):
        if(self.treeview.focus() != ""):
            if messagebox.askyesno(message="¿Realmente quieres modifcar este registro?", title = "Alerta") == True:
                sql = "select * from Genero where idGenero = %(idGenero)s"
                row_data = self.libreria.run_select_filter(sql, {"idGenero": self.treeview.focus()})[0]
                modificar_genero(self.libreria, self, row_data)

    def eliminar_genero(self):
        if(self.treeview.focus() != ""):
            if messagebox.askyesno(message="¿Realmente quieres eliminar este registro?", title = "Alerta") == True:
                sql = "delete from Genero where idGenero = %(idGenero)s"
                self.libreria.run_sql(sql, {"idGenero": self.treeview.focus()})
                self.llenar_treeview_genero()


class insertar_genero:
    def __init__(self, db, padre):
        self.padre = padre
        self.libreria = db
        self.insert_datos = tk.Toplevel()
        self.__config_window()
        self.__config_label()
        self.__config_entry()
        self.__config_button()

    def __config_window(self):
        self.insert_datos.geometry('300x200')
        self.insert_datos.title("Insertar Género")
        self.insert_datos.resizable(width=0, height=0)

    def __config_label(self):
        #tk.Label(self.insert_datos, text = "idGenero: ").place(x = 10, y = 10, width = 80, height = 20)
        tk.Label(self.insert_datos, text = "Nombre: ").place(x = 10, y = 10, width = 80, height = 30)

    def __config_entry(self):
        #self.entry_idGenero = tk.Entry(self.insert_datos)
        #self.entry_idGenero.place(x = 110, y = 10, width = 80, height = 20)
        self.entry_nombre = tk.Entry(self.insert_datos)
        self.entry_nombre.place(x = 100, y = 10, width = 180, height = 30)

    def __config_button(self):
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.__insertar).place(x=0, y =170, width = 300, height = 30)

    def __insertar(self): #Insercion en la base de datos.
        sql = """insert into Genero (nombre) values (%(nombre)s)"""
        self.libreria.run_sql(sql, {"nombre": self.entry_nombre.get()})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_genero()

class modificar_genero:
    def __init__(self, db, padre, row_data):
        self.padre = padre
        self.libreria = db
        self.row_data = row_data
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self): #Settings
        self.insert_datos.geometry('300x200')
        self.insert_datos.title("Modificar Género")
        self.insert_datos.resizable(width=0, height=0)

    def config_label(self): #Labels
        tk.Label(self.insert_datos, text = "Nombre: ").place(x = 10, y = 50, width = 80, height = 30)

    def config_entry(self):#Se configuran los inputs
        self.entry_nombre = tk.Entry(self.insert_datos)
        self.entry_nombre.place(x = 100, y = 50, width = 180, height = 30)
        self.entry_nombre.insert(0, self.row_data[1])

    def config_button(self): #Botón aceptar, llama a la función modificar cuando es clickeado.
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.modificar).place(x=0, y =170, width = 300, height = 30)

    def modificar(self): #Insercion en la base de datos.
        sql = """update Genero set nombre = %(nombre)s
                where idGenero = %(idGenero)s"""
        self.libreria.run_sql(sql, {"idGenero": int(self.row_data[0]),
            "nombre": self.entry_nombre.get()})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_genero()
