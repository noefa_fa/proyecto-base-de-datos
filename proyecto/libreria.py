# Librerias
import tkinter as tk
from tkinter import ttk
from tkinter import Menu
from tkinter import LabelFrame, Label, Frame
from tkinter import Button
from PIL import Image, ImageTk
from tkinter import messagebox


# Archivos que se utilizaran
from database import Database
from libro import libro
from genero import genero
from editorial import editorial
from autor import autor
from empleado import empleado
from cliente import cliente
from ejemplar import ejemplar
from compra import compra
from ejemplar_participa_compra import ejemplar_compra
from informacion import informacion
from histograma import histograma
from scatter import scatter
from histograma_1 import histograma_1
from vista1 import vista1
from view2 import view2
from libro_autor import libro_autor
from trigger1 import trigger1
from trigger2 import trigger2
from funcion1 import funcion1
from funcion2 import funcion2
from dinamica1 import dinamica1
from dinamica2 import dinamica2



class libreria:
    def __init__(self, db):
        self.libreria = db

        # Main window
        self.root = tk.Tk()

        # Algunas especificaciones de tamaño y título de la ventana
        self.root.geometry("1000x800")
        self.root.title("App Librería Noefa")
        self.root.config(bg="#8a4821")
        inicio(self.libreria, self.root)

class inicio:
    def __init__(self, db, root):
        self.libreria = db
        self.root = root

        # Main window
        self.root.config(bg="#8a4821")

        self.__agrega_imagen_principal()
        self.__crea_botones_principales()

        # Empieza a correr la interfaz.
        self.root.mainloop()

    # imagen principal.
    def __agrega_imagen_principal(self):
        #
        self.frame = LabelFrame(self.root, text="", relief=tk.FLAT, bg="#8a4821")
        self.frame.place(x=0, y=0, relwidth=1, relheight=1)

        image = Image.open("app.jpg")
        photo = ImageTk.PhotoImage(image.resize((950, 550), Image.ANTIALIAS))
        self.label = Label(self.frame, image=photo)
        self.label.image = photo
        self.label.pack()

    # botones principales.
    def __crea_botones_principales(self):
        self.b_in = tk.Button ( self.root, text="Ingresar", width=20, command=self.btn_hide)
        self.b_in.place ( x=373.39, y=600, width=205, height=48)
        self.b_in.bind ( '<Button-1>', self.__login )
        self.b_in.config(bg="#8a4821")


    def btn_hide(self):
        self.b_in.place_forget ()

    def __login(self, button):
        self.label.master.destroy ()
        login(self.libreria, self.root)

class login:
    def __init__(self, db, root):
        self.db = db
        self.root = root

        self.frame = LabelFrame(self.root, text="", relief=tk.FLAT, bg="#8a4821")
        self.frame.place(x=0, y=0, relwidth=1, relheight=1)
        image = Image.open ("login.jpg" )
        photo = ImageTk.PhotoImage(image.resize((950, 550), Image.ANTIALIAS))
        self.label = Label ( self.frame, image=photo )
        self.label.image = photo
        self.label.pack ()

        #Contenido Ventana
        self.__config_entry()
        self.__config_buttons()

    #Configuración de las casillas que el usuario ingresa info
    def __config_entry(self):
        self.entry_user = ttk.Label(self.frame, text="Administrador")
        self.entry_user.place(x = 450, y = 385, width = 200, height = 30)
        self.entry_pass = ttk.Entry ( self.frame, show="*" )
        self.entry_pass.place ( x=510, y=420.5, width=150, height=30 )
        #self.entry_user.insert(0, "Administrador")

    #Configuración de los botones
    def __config_buttons(self):
        ttk.Button(self.frame, text="Aceptar", command = self.__eleccion).place (x=380, y=550, width=200, height=30)

    def __eleccion(self):
        if (self.entry_pass.get() != "admin"):
              self.mensaje()
        else:
            self.ufa()

    def mensaje(self):
        messagebox.showinfo (self.root, message="Contraseña incorrecta" )

    def ufa(self):
        self.frame.place_forget ()
        administrador (self.db, self.root)

class administrador:
    def __init__(self, db, root):
        self.libreria = db
        self.root = root
        # botones e imagen
        self.__crea_menubar()
        self.__agrega_imagen_principal()
        self.__crea_botones_principales()
        # Empieza a correr la interfaz.
        self.root.mainloop()

    # menubar
    def __crea_menubar(self):
        menubar = Menu(self.root, bg="#8a4821")
        self.root.config(menu=menubar)

        # Dentro del menu esta la opcion ayuda que contrendra informacion de la app
        maestros_menu = Menu(menubar, font=("Time new roman", 12), tearoff=0, bg="#8a4821")
        menubar.add_cascade(label="Ayuda", font=("Time new roman", 12), menu=maestros_menu)
        maestros_menu.add_command(label = "Acerca de app", font=("Time new roman", 12), command = self.informacion)

        # Dentro del menú está la opción de vistas
        menuresumen = Menu(menubar, tearoff = 0, bg="#8a4821")
        menubar.add_cascade(label = "Vistas", font=("Time new roman", 12), menu = menuresumen)
        # Se muestra vistas del resumen de libro...
        menuresumen.add_command(label = "View Libros", font=("Time new roman", 12), command= self.vista1)
        menuresumen.add_command(label = "View Empleado", font=("Time new roman", 12), command= self.vista2)

        # Dentro del menú está la opción de gráficos
        graficos = Menu(menubar, font=("Time new roman", 12), tearoff=0, bg="#8a4821")
        menubar.add_cascade(label = "Gráficos", font=("Time new roman", 12), menu=graficos)
        # Se muestra los graficos de ...
        graficos.add_command(label = "Histograma géneros",  font=("Time new roman", 12), command = self.histograma)
        graficos.add_command(label = "Distribución venta", font=("Time new roman", 12), command = self.scatter)
        graficos.add_command(label = "Histograma ejemplares", font = ("Time new roman", 12), command = self.histograma_1)

        trigger = Menu(menubar, font=("Time new roman", 12), tearoff=0, bg="#8a4821")
        menubar.add_cascade(label = "Triggers", font=("Time new roman", 12), menu=trigger)
        # Se muestran los triggers
        trigger.add_command(label = "Empleados llegada", font=("Time new roman", 12), command = self.trigger1)
        trigger.add_command(label = "Cambio precios", font=("Time new roman", 12), command = self.trigger2)

        funcion = Menu(menubar, font=("Time new roman", 12), tearoff=0, bg="#8a4821")
        menubar.add_cascade(label = "Funciones", font=("Time new roman", 12), menu=funcion)
        # Se muestran las funciones
        funcion.add_command(label = "Descuento", font=("Time new roman", 12), command = self.funcion1)
        funcion.add_command(label = "Libros por editorial", font=("Time new roman", 12), command = self.funcion2)

        dinamica = Menu(menubar, font=("Time new roman", 12), tearoff=0, bg="#8a4821")
        menubar.add_cascade(label = "Dinámicas", font=("Time new roman", 12), menu=dinamica)
        # Se muestran las dinamicas
        dinamica.add_command(label = "Libros por precio", font=("Time new roman", 12), command = self.dinamica1)
        dinamica.add_command(label = "Libros vendidos", font=("Time new roman", 12), command = self.dinamica2)

    # imagen principal.
    def __agrega_imagen_principal(self):
        #
        frame = LabelFrame(self.root, text="Librería Noefa", font=("Time new roman", 14), relief=tk.FLAT, bg="#8a4821")
        frame.place(x=235, y=10, relwidth=0.68, relheight=0.95)

        image = Image.open("libreria.jpg")
        photo = ImageTk.PhotoImage(image.resize((600, 400), Image.ANTIALIAS))
        label = Label(frame, image=photo)
        label.image = photo
        label.pack()

    # botones principales.
    def __crea_botones_principales(self):
        padx = 10
        pady = 10

        #
        frame = LabelFrame(self.root, text="Opciones administrador", font=("Time new roman", 11), relief=tk.GROOVE, bg="#8a4821")
        frame.place(x=30, y=10, width=200, relheight=0.95)

        #
        b1 = Button(frame, text="Libro", font=("Time new roman", 12), width=15, bg="#b5947f")
        b1.grid(row=0, column=0, padx=padx, pady=pady)
        b1.bind('<Button-1>', self.__mostrar_libro)

        b2 = Button(frame, text="Autor", font=("Time new roman", 12), width=15, bg="#b5947f")
        b2.grid(row=1, column=0, padx=padx, pady=pady)
        b2.bind('<Button-1>', self.__mostrar_autor)

        b3 = Button(frame, text="Géneros", font=("Time new roman", 12), width=15, bg="#b5947f")
        b3.grid(row=2, column=0, padx=padx, pady=pady)
        b3.bind('<Button-1>', self.__mostrar_genero)

        b4 = Button(frame, text="Editorial", font=("Time new roman", 12), width=15, bg="#b5947f")
        b4.grid(row=3, column=0, padx=2, pady=pady)
        b4.bind('<Button-1>', self.__mostrar_editorial)

        b5 = Button(frame, text="Empleado", font=("Time new roman", 12), width=15, bg="#b5947f")
        b5.grid(row=4, column=0, padx=2, pady=pady)
        b5.bind('<Button-1>', self.__mostrar_empleado)

        b6 = Button(frame, text="Cliente", font=("Time new roman", 12), width=15, bg="#b5947f")
        b6.grid(row=5, column=0, padx=2, pady=pady)
        b6.bind('<Button-1>', self.__mostrar_cliente)

        b7 = Button(frame, text="Ejemplar", font=("Time new roman", 12), width=15, bg="#b5947f")
        b7.grid(row=6, column=0, padx=2, pady=pady)
        b7.bind('<Button-1>', self.__mostrar_ejemplar)

        b8 = Button(frame, text="Compra", font=("Time new roman", 12), width=15, bg="#b5947f")
        b8.grid(row=7, column=0, padx=2, pady=pady)
        b8.bind('<Button-1>', self.__mostrar_compra)

        b9 = Button(frame, text="Detalle venta", font=("Time new roman", 12), width=15, bg="#b5947f")
        b9.grid(row=8, column=0, padx=2, pady=pady)
        b9.bind('<Button-1>', self.__mostrar_detalleventa)

        b10 = Button(frame, text="Relación autor-libro", font=("Time new roman", 12), width=15, bg="#b5947f")
        b10.grid(row=9, column=0, padx=2, pady=pady)
        b10.bind('<Button-1>', self.__mostrar_autorlibro)

    def informacion(self):
        informacion(self.root, self.libreria)

    def vista1(self):
        vista1(self.root, self.libreria)

    def vista2(self):
        view2(self.root, self.libreria)

    def histograma(self):
        histograma(self.root, self.libreria)

    def histograma_1(self):
        histograma_1(self.root, self.libreria)

    def scatter(self):
        scatter(self.root, self.libreria)

    def trigger1(self):
        trigger1(self.root, self.libreria)

    def trigger2(self):
        trigger2(self.root, self.libreria)

    def funcion1(self):
        funcion1(self.root, self.libreria)

    def funcion2(self):
        funcion2(self.root, self.libreria)

    def dinamica1(self):
        dinamica1(self.root, self.libreria)

    def dinamica2(self):
        dinamica2(self.root, self.libreria)

    # muestra ventana libro
    def __mostrar_libro(self, button):
        libro(self.root, self.libreria)

    # muestra ventana editorial
    def __mostrar_editorial(self, button):
        editorial(self.root, self.libreria)

    def __mostrar_genero(self, button):
        genero(self.root, self.libreria)

    def __mostrar_autor(self, button):
        autor(self.root, self.libreria)

    def __mostrar_empleado(self, button):
        empleado(self.root, self.libreria)

    def __mostrar_cliente(self, button):
        cliente(self.root, self.libreria)

    def __mostrar_ejemplar(self, button):
        ejemplar(self.root, self.libreria)

    def __mostrar_compra(self, button):
        compra(self.root, self.libreria)

    def __mostrar_detalleventa(self, button):
        ejemplar_compra(self.root, self.libreria)

    def __mostrar_autorlibro(self, button):
        libro_autor(self.root, self.libreria)

def main():
    # Conecta a la base de datos
    db = Database()

    # La app xD
    libreria(db)

if __name__ == "__main__":
    main()
