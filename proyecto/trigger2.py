import tkinter as tk
from tkinter import ttk
from tkinter import Button


class trigger2:
    def __init__(self, root, db):
        self.libreria = db
        self.data = []
        # Ventana emergente
        self.root = tk.Toplevel()
        # Ajustes de ventana
        self.root.geometry('500x300')
        self.root.title("Cambio de precios")
        self.root.resizable(width = 0, height = 0)
        # toplevel modal
        self.root.transient(root)


        # Funcionalidades
        self.__config_treeview_trigger()

        # Crear trigger
        self.generar_trigger()

        self.llenar_treeview_trigger()

    def generar_trigger(self):
        if(self.treeview.focus() != ""):
            sql = """create table cambio_precio (id_cambio int not null
                    primary key auto_increment, idLibro int, titulo varchar (200),
                    precio int, precio_cambio int, fecha date);
                    delimiter //
                    create trigger trigger_cambio_precio after update on Libro
                    for each row begin insert into cambio_precio (idLibro,
                    titulo, precio, precio_cambio, fecha) values (NEW.idLibro,
                    NEW.titulo, OLD.precio, NEW.precio, current_date); end;//
                    delimiter ; """
            self.libreria.run_sql(sql, {"idEmpleado": self.treeview.focus()})


    def __config_treeview_trigger(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#0", "#1", "#2", "#3", "#4"))
        self.treeview.heading("#0", text = "Id")
        self.treeview.heading("#1", text = "Título")
        self.treeview.heading("#2", text = "Precio")
        self.treeview.heading("#3", text = "Cambio precio")
        self.treeview.heading("#4", text = "Fecha cambio")
        self.treeview.column("#0", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#1", minwidth = 135, width = 135, stretch = False)
        self.treeview.column("#2", minwidth = 135, width = 135, stretch = False)
        self.treeview.column("#3", minwidth = 135, width = 135, stretch = False)
        self.treeview.column("#4", minwidth = 135, width = 135, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 300, width = 500)
        self.llenar_treeview_trigger()
        self.root.after(0, self.llenar_treeview_trigger)

    def llenar_treeview_trigger(self):
        sql = """select idLibro, titulo, precio, precio_cambio, fecha from cambio_precio;"""

        # Guarda info obtenida tras la consulta
        data = self.libreria.run_select(sql)

        # Evalúa si el contenido de la tabla en la app es distinto al de la db
        if(data != self.data):
            # Elimina todos los rows del treeview si hay diferencias
            self.treeview.delete(*self.treeview.get_children())

            for i in data:
                # Inserta valores en treeview
                self.treeview.insert("", "end", text = i[0],
                    values = (i[1], i[2], i[3], i[4]))

            self.data = data
