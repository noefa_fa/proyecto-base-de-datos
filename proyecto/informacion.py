import tkinter as tk
from tkinter import ttk
from tkinter import Button
from tkinter import Message

# Se define la clase informacion que mostrara mas detalles acerca de la app al usuario
class informacion:
    def __init__(self, root, db):
        # Se actualiza atributo con la database
        self.db = db
        self.data = []
        self.root = tk.Toplevel()
        # Información de ventana
        self.root.geometry('300x250')
        self.root.title("Acerca de la App")
        self.root.config(bg="#b5947f")
        self.root.resizable(width = 0, height = 0)
        self.root.transient(root)

        # Se llama a cada una de las funciones que permiten su funcionamiento
        self.boton()
        self.mensaje()

    # Se crea la funcion que permite el funcionamiento del boton aceptar
    def boton(self):
        boton_aceptar = Button(self.root, text="Aceptar", command=self.root.destroy, font=("Time new roman", 12), bg="#b5947f", fg='black')
        # Ubicacion del boton
        boton_aceptar.place(x=110, y= 150)

    # Esta funcion que crea el mensaje
    def mensaje(self):
        # se imprime el mensaje
        informacion = Message(self.root,text = "Versión 2.0.2.1 Desarrolladoras: Noemí Gómez          Josefa Hillmer            Son propietarias de Librería Noefa", font=("Time new roman", 12), bg="#b5947f")
        informacion.pack()
        self.root.mainloop()
