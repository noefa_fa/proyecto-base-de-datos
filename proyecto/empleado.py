import tkinter as tk
from tkinter import ttk
from tkinter import messagebox

class empleado:
    def __init__(self, root, db):
        self.libreria = db
        self.data = []

        #Toplevel es una ventana que está un nivel arriba que la principal
        self.root = tk.Toplevel()
        self.root.geometry('750x400')
        self.root.title("Empleados")
        self.root.resizable(width=0, height=0)

        # toplevel modal
        self.root.transient(root)

        #
        self.__config_treeview_empleado()
        self.__config_buttons_empleado()

    def __config_treeview_empleado(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1", "#2", "#3"))
        self.treeview.heading("#0", text = "Id")
        self.treeview.heading("#1", text = "Nombre")
        self.treeview.heading("#2", text = "Apellido")
        self.treeview.heading("#3", text = "Teléfono")
        self.treeview.column("#0", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#1", minwidth = 220, width = 220, stretch = False)
        self.treeview.column("#2", minwidth = 220, width = 220, stretch = False)
        self.treeview.column("#3", minwidth = 220, width = 220, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 350, width = 750)
        self.llenar_treeview_empleado()
        self.root.after(0, self.llenar_treeview_empleado)

    def __config_buttons_empleado(self):
        tk.Button(self.root, text="Insertar empleado",
            command = self.insertar_empleado).place(x = 0, y = 350, width = 250, height = 50)
        tk.Button(self.root, text="Modificar empleado",
            command = self.modificar_empleado).place(x = 250, y = 350, width = 250, height = 50)
        tk.Button(self.root, text="Eliminar empleado",
            command = self.eliminar_empleado).place(x = 500, y = 350, width = 250, height = 50)

    def llenar_treeview_empleado(self):
        sql = "select * from Empleado"
        # Ejecuta el select
        data = self.libreria.run_select(sql)

        # Si la data es distina a la que hay actualmente...
        if(data != self.data):
            # Elimina todos los rows del treeview
            self.treeview.delete(*self.treeview.get_children())#Elimina todos los rows del treeview
            for i in data:
                #Inserta los datos
                self.treeview.insert("", "end", text = i[0],
                    values = (i[1], i[2], i[3]), iid = i[0])
            self.data = data  #Actualiza la data


    def insertar_empleado(self):
        insertar_empleado(self.libreria, self)

    def modificar_empleado(self):
        if(self.treeview.focus() != ""):
            if messagebox.askyesno(message="¿Realmente quieres modificar este registro?", title = "Alerta") == True:
                sql = "select * from Empleado where idEmpleado = %(idEmpleado)s"
                row_data = self.libreria.run_select_filter(sql, {"idEmpleado": self.treeview.focus()})[0]
                modificar_empleado(self.libreria, self, row_data)

    def eliminar_empleado(self):
        if(self.treeview.focus() != ""):
            if messagebox.askyesno(message="¿Realmente quieres eliminar este registro?", title = "Alerta") == True:
                sql = "delete from Empleado where idEmpleado = %(idEmpleado)s"
                self.libreria.run_sql(sql, {"idEmpleado": self.treeview.focus()})
                self.llenar_treeview_empleado()


class insertar_empleado:
    def __init__(self, db, padre):
        self.padre = padre
        self.libreria = db
        self.insert_datos = tk.Toplevel()
        self.__config_window()
        self.__config_label()
        self.__config_entry()
        self.__config_button()

    def __config_window(self):
        self.insert_datos.geometry('300x200')
        self.insert_datos.title("Insertar empleado")
        self.insert_datos.resizable(width=0, height=0)

    def __config_label(self):
        #tk.Label(self.insert_datos, text = "idempleado: ").place(x = 10, y = 10, width = 80, height = 20)
        tk.Label(self.insert_datos, text = "Nombre: ").place(x = 10, y = 10, width = 80, height = 30)
        tk.Label(self.insert_datos, text = "Apellido: ").place(x = 10, y = 50, width = 80, height = 30)
        tk.Label(self.insert_datos, text = "Teléfono: ").place(x = 10, y = 90, width = 80, height = 30)

    def __config_entry(self):
        #self.entry_idempleado = tk.Entry(self.insert_datos)
        #self.entry_idempleado.place(x = 110, y = 10, width = 80, height = 20)
        self.entry_nombre = tk.Entry(self.insert_datos)
        self.entry_nombre.place(x = 100, y = 10, width = 180, height = 30)
        self.entry_apellido = tk.Entry(self.insert_datos)
        self.entry_apellido.place(x = 100, y = 50, width = 180, height = 30)
        self.entry_telefono = tk.Entry(self.insert_datos)
        self.entry_telefono.place(x = 100, y = 90, width = 180, height = 30)

    def __config_button(self):
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.__insertar).place(x=0, y =170, width = 300, height = 30)

    def __insertar(self): #Insercion en la base de datos.
        sql = """insert into Empleado (nombre, apellido, telefono)
                values (%(nombre)s, %(apellido)s, %(telefono)s)"""
        self.libreria.run_sql(sql, {"nombre": self.entry_nombre.get(),
            "apellido": self.entry_apellido.get(),
            "telefono": self.entry_telefono.get()})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_empleado()

class modificar_empleado:
    def __init__(self, db, padre, row_data):
        self.padre = padre
        self.libreria = db
        self.row_data = row_data
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self): #Settings
        self.insert_datos.geometry('300x200')
        self.insert_datos.title("Modificar empleado")
        self.insert_datos.resizable(width=0, height=0)

    def config_label(self): #Labels
    #    tk.Label(self.insert_datos, text = "idEmpleado: ").place(x = 10, y = 10, width = 80, height = 30)
        tk.Label(self.insert_datos, text = "Nombre: ").place(x = 10, y = 50, width = 80, height = 30)
        tk.Label(self.insert_datos, text = "Apellido: ").place(x = 10, y = 90, width = 80, height = 30)
        tk.Label(self.insert_datos, text = "Teléfono: ").place(x = 10, y = 130, width = 80, height = 30)

    def config_entry(self):#Se configuran los inputs
        #self.entry_idEmpleado = tk.Entry(self.insert_datos)
        #self.entry_idEmpleado.place(x = 100, y = 10, width = 180, height = 30)
        self.entry_nombre = tk.Entry(self.insert_datos)
        self.entry_nombre.place(x = 100, y = 50, width = 180, height = 30)
        self.entry_apellido = tk.Entry(self.insert_datos)
        self.entry_apellido.place(x = 100, y = 90, width = 180, height = 30)
        self.entry_telefono = tk.Entry(self.insert_datos)
        self.entry_telefono.place(x = 100, y = 130, width = 180, height = 30)
    #    self.entry_idEmpleado.insert(0, self.row_data[0])
        self.entry_nombre.insert(0, self.row_data[1])
        self.entry_apellido.insert(0, self.row_data[2])
        self.entry_telefono.insert(0, self.row_data[3])

    def config_button(self): #Botón aceptar, llama a la función modificar cuando es clickeado.
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.modificar).place(x=10, y =170, width = 300, height = 30)

    def modificar(self): #Insercion en la base de datos.
        sql = """update Empleado set nombre = %(nombre)s,
                apellido = %(apellido)s, telefono = %(telefono)s
                where idEmpleado = %(idEmpleado)s"""
        self.libreria.run_sql(sql, {"idEmpleado": int(self.row_data[0]),
        #    "idEmpleadon": self.entry_idEmpleado.get(),
            "nombre": self.entry_nombre.get(),
            "apellido": self.entry_apellido.get(),
            "telefono": self.entry_telefono.get()})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_empleado()
