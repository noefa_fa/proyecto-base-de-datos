import tkinter as tk
from tkinter import ttk
from tkinter import messagebox

from libro import libro
from autor import autor

class libro_autor:
    def __init__(self, root, db):
        self.libreria = db
        self.data = []

        #Toplevel es una ventana que está un nivel arriba que la principal
        self.root = tk.Toplevel()
        self.root.geometry('750x400')
        self.root.title("Libros")
        self.root.resizable(width=0, height=0)

        # toplevel modal
        self.root.transient(root)

        #
        self.__config_treeview_libro_autor()
        self.__config_buttons_libro_autor()

    def __config_treeview_libro_autor(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1", "#2", "#3"))
        self.treeview.heading("#0", text = "idLibro")
        self.treeview.heading("#1", text = "idAutor")
        self.treeview.heading("#2", text = "Libro")
        self.treeview.heading("#3", text = "Autor")
        self.treeview.column("#0", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#1", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#2", minwidth = 350, width = 350, stretch = False)
        self.treeview.column("#3", minwidth = 200, width = 200, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 350, width = 750)
        self.llenar_treeview_libro_autor()
        self.root.after(0, self.llenar_treeview_libro_autor)

    def __config_buttons_libro_autor(self):
        tk.Button(self.root, text="Insertar",
            command = self.__insertar_libro_autor).place(x = 0, y = 350, width = 250, height = 50)
        tk.Button(self.root, text="Modificar",
            command = self.__modificar_libro_autor).place(x = 250, y = 350, width = 250, height = 50)
        tk.Button(self.root, text="Eliminar",
            command = self.__eliminar_libro_autor).place(x = 500, y = 350, width = 250, height = 50)

    def llenar_treeview_libro_autor(self):
        sql = """select Libro_idLibro, Autor_idAutor,
                Libro.titulo as Titulo,
                concat(Autor.nombre, " ", Autor.apellido, " ", Autor.pseudonimo)
                from Libro join Autor join Libro_has_Autor
                on Libro_has_Autor.Autor_idAutor = Autor.idAutor
                and Libro_has_Autor.Libro_idLibro = Libro.idLibro;"""

        # Ejecuta el select
        data = self.libreria.run_select(sql)

        # Si la data es distina a la que hay actualmente...
        if(data != self.data):
            # Elimina todos los rows del treeview
            self.treeview.delete(*self.treeview.get_children())#Elimina todos los rows del treeview
            for i in data:
                #Inserta los datos
                self.treeview.insert("", "end", text = i[0],
                    values = (i[1], i[2], i[3]), iid = i[0])
            self.data = data  #Actualiza la data


    def __insertar_libro_autor(self):
        insertar_libro_autor(self.libreria, self)


    def __modificar_libro_autor(self):
        if(self.treeview.focus() != ""):
            messagebox.showwarning(message="Ambas columnas son claves primarias",
                                            title = "No se puede modificar")
                #row_data = self.libreria.run_select_filter(sql, {"idLibro": self.treeview.focus()})[0]
                #modificar_libro(self.libreria, self, row_data)

    def __eliminar_libro_autor(self):
        if(self.treeview.focus() != ""):
            # Lista que contiene las columanas de todo el registro seleccionado
            item1 = (self.treeview.item(self.treeview.focus()))["text"]
            item2 = (self.treeview.item(self.treeview.focus()))["values"]
            #print("ITEM: ", item1, item2)
            # Se obtiene las claves primarias
            self.claves = [item1, item2[0]]
        #    self.first = item1
            #print("CLAVES: ", self.claves)
            if messagebox.askyesno(message="¿Realmente quieres eliminar este registro?", title = "Alerta") == True:
                sql = """delete from Libro_has_Autor where
                        Libro_idLibro = %(idLibro)s and Autor_idAutor = %(idAutor)s """
                self.libreria.run_sql(sql, {"idLibro": self.claves[0], "idAutor": self.claves[1]})
                self.llenar_treeview_libro_autor()


class insertar_libro_autor:
    def __init__(self, db, padre):
        self.padre = padre
        self.libreria = db
        self.insert_datos = tk.Toplevel()
        self.__config_window()
        self.__config_label()
        self.__config_entry()
        self.__config_button()

    def __config_window(self):
        self.insert_datos.geometry('300x200')
        self.insert_datos.title("Insertar libro")
        self.insert_datos.resizable(width=0, height=0)

    def __config_label(self):
        tk.Label(self.insert_datos, text = "idLibro ").place(x = 10, y = 10, width = 80, height = 30)
        tk.Label(self.insert_datos, text = "idAutor: ").place(x = 10, y = 50, width = 80, height = 30)

    def __config_entry(self):
        self.combo_idl = ttk.Combobox(self.insert_datos)
        self.combo_idl.place(x = 100, y = 10, width = 180, height= 30)
        self.combo_idl["values"], self.idsg = self.__fill_combo_idl()
        self.combo_ida= ttk.Combobox(self.insert_datos)
        self.combo_ida.place(x = 100, y = 50, width = 180, height= 30)
        self.combo_ida["values"], self.idse = self.__fill_combo_ida()

    def __config_button(self):
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.__insertar).place(x=0, y =170, width = 300, height = 30)

    def __fill_combo_idl(self):
        sql = "select idLibro from Libro"
        self.data = self.libreria.run_select(sql)
        return [i[0] for i in self.data], [i[0] for i in self.data]

    def __fill_combo_ida(self):
        sql = "select idAutor from Autor"
        self.data = self.libreria.run_select(sql)
        return [i[0] for i in self.data], [i[0] for i in self.data]

    def __insertar(self): #Insercion en la base de datos.
        sql = """insert into Libro_has_Autor (Libro_idLibro, Autor_idAutor)
                values (%(idLibro)s, %(idAutor)s)
                """
        #print(self.ids[self.combo_idl.current()])
        self.libreria.run_sql(sql, {"idLibro": self.idsg[self.combo_idl.current()],
                    "idAutor": self.idse[self.combo_ida.current()]})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_libro_autor()
