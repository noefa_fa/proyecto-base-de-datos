import tkinter as tk
from tkinter import ttk
import matplotlib.pyplot as plot
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

# Se crea una clase de histograma
class histograma_1:
    def __init__(self, root, db):
        self.root = root
        self.libreria = db
        # Se define la ventana que mostrara el histograma
        self.graph = tk.Toplevel()
        self.graph.geometry('1000x600')
        self.graph.title("Histograma")

        # se llama a la funcion que permite la configuracion de la grafica
        self.__config_grafica()

    # Se define la funcion de la configuracion de grafica
    def __config_grafica(self):
        # Figura que hara el plot
        figura_canva, ax = plot.subplots()
        x, y = self.obtencion_datos()
        plot.xticks(rotation=90)
        # Titulo
        plot.title("Histograma: Cantidad de ejemplares por libro")
        # Nombre del eje X
        plot.xlabel("Libros")
        # Nombre del eje Y
        plot.ylabel("Cantidad de ejemplares")
        # Color de las barras
        plot.bar(x, y, color = "#9c3d03")
        canvas = tk.Canvas(self.graph, width=1000, height=1000, background="white")
        canvas = FigureCanvasTkAgg(figura_canva, master = self.graph)
        canvas.draw()
        canvas.get_tk_widget().pack()


# Funcion que permite obtener los datos desde sql
    def obtencion_datos(self):
        sql = """select Libro.titulo, count(idEjemplares)
                from Ejemplar join Libro where Libro_idLibro = Libro.idLibro
                group by Libro_idLibro;"""

        data = self.libreria.run_select(sql)
        # el eje X
        x = [i[0] for i in data]
        #xticks(rotation=90)
        # el eje Y
        y = [i[1] for i in data]
        return x, y
