
# Importan librerías
import tkinter as tk
from tkinter import ttk
from tkinter import Button
from tkinter import messagebox
from tkinter import IntVar

# Clase para realizar consulta dinámica
class dinamica1:
    def __init__(self, root, db):
        # Se actualiza atributo con la database
        self.libreria = db
        self.data = []

        # Se crea una nueva ventana superior a la principal
        self.root = tk.Toplevel()
        # Se define el tamaño de la ventana
        self.root.geometry('300x200')
        # Se define el título de la ventana
        self.root.title("Filtrar libros por precio")
        # Esta opción permite cambiar el tamano de la venta
        self.root.resizable(width = 0, height = 0)
        self.root.transient(root)

        # Widgets a usar
        self.__config_button()
        self.__config_label()
        self.__config_entry()

    def __config_button(self):
        # Botón para realizar la consulta y generar tabla
        btn_ok = tk.Button(self.root, text = "Consultar", command = self.__query_dinamica)
        btn_ok.place(x = 50, y = 160, width = 80, height = 20)

        # Botón para cancelar la consulta
        btn_cancel = tk.Button(self.root, text = "Cancelar")
        btn_cancel.place(x = 170, y = 160, width = 80, height = 20)

    def __config_label(self):
        # Definición de entradas de texto
        min = tk.Label(self.root, text = "Mínimo: ")
        min.place(x = 10, y = 50, width = 160, height = 30)
        max = tk.Label(self.root, text = "Máximo: ")
        max.place(x = 10, y = 100, width = 160, height = 30)

    def __config_entry(self):
        # Recibe filtro menor o igual a
        self.min = tk.Entry(self.root)
        self.min.place(x = 180, y = 50, width = 60, height = 30)

        # Recibe filtro menor o igual a
        self.max = tk.Entry(self.root)
        self.max.place(x = 180, y = 100, width = 60, height = 30)

    def __query_dinamica(self):

        sql = """select * from Libro where precio between %(min)s and %(max)s;"""

        # Se obtienen resultados de la consulta
        self.libreria.run_select_filter(sql, {"min": self.min.get(),
                                            "max": self.max.get()})
        libro(self.libreria, self.min.get(), self.max.get())


class libro:
    def __init__(self, db, min, max):
        self.libreria = db
        self.data = []
        self.min = min
        self.max = max

        # Ventana emergente
        self.tabla = tk.Toplevel()

        # Ajustes de ventana
        self.tabla.geometry('500x300')
        texto_titulo = "Libros"
        self.tabla.title(texto_titulo)
        self.tabla.resizable(width = 0, height = 0)

        #  Configuración del treeview
        self.__config_treeview_filtro()

    def __config_treeview_filtro(self):
        self.treeview = ttk.Treeview(self.tabla)
        # Configuración de títulos de columnas
        self.treeview.configure(columns = ("#0", "#1", "#2"))
        self.treeview.heading("#0", text = "idLibro")
        self.treeview.heading("#1", text = "Título")
        self.treeview.heading("#2", text = "Precio")
        # Configuración de tamaños de cada columna
        self.treeview.column("#0", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#1", minwidth = 280, width = 280, stretch = False)
        self.treeview.column("#2", minwidth = 140, width = 140, stretch = False)
        # Ubica treeview
        self.treeview.place(x = 0, y = 0, height = 350, width = 850)
        # Llenado del treeview
        self.llenar_treeview_filtro()
        self.tabla.after(0, self.llenar_treeview_filtro)

    def llenar_treeview_filtro(self):
        sql = """select idLibro, titulo, precio from Libro where precio between %(min)s and %(max)s;"""

        # Se actualiza data con el resultado de la query dinámica
        data = self.libreria.run_select_filter(sql, {"min": self.min,
                                                    "max": self.max})

        # Evalúa si el contenido de la tabla en la app es distinto al de la db
        if(data != self.data):
            # Elimina todos los rows del treeview si hay diferencias
            self.treeview.delete(*self.treeview.get_children())

            # Recorre cada registro (tupla) guardado en var data
            for i in data:
                # Inserta valores en treeview
                self.treeview.insert("", "end", text = i[0], values = i[1:3])

            self.data = data
