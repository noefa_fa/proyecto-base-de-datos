# Importan librerías principales a usar en Tkinter
import tkinter as tk
from tkinter import ttk
from tkinter import Button
from tkinter import messagebox
from tkinter import IntVar

class funcion2:
    def __init__(self, root, db):
        # Se actualiza atributo con la database
        self.libreria = db
        self.data = []

        # Se crea una nueva ventana superior a la principal
        self.root = tk.Toplevel()
        # Se define el tamaño de la ventana
        self.root.geometry('300x270')
        # Se define el título de la ventana
        self.root.title("Libros por editorial")
        # Esta opción permite cambiar el tamano de la venta
        self.root.resizable(width = 0, height = 0)
        self.root.transient(root)

        # Widgets
        self.__config_button()
        self.__config_label()
        self.__config_entry()


    def __config_label(self):
        # Definición de entradas de texto
        editorial = tk.Label(self.root, text = "Editorial: ")
        editorial.place(x = 0, y = 30, width = 140, height = 30)
        can_libros =  tk.Label(self.root, text = "Cantidad de libros: ")
        can_libros.place(x = 0, y = 90, width = 140, height = 30)

    def __config_entry(self):
        self.cont = 0;
        self.combo = ttk.Combobox(self.root)
        self.combo.place(x = 110, y = 30, width = 140, height= 30)
        self.combo["values"], self.ids = self.combo_editorial()
        self.entry_user = ttk.Label(self.root, text=self.cont)
        self.entry_user.place(x = 170, y = 90, width = 140, height = 30)

    def combo_editorial(self):
        row_data = """select idEditorial, nombre from Editorial;"""
        self.data = self.libreria.run_select(row_data)
        # Se muestran los generos
        return [i[1] for i in self.data], [i[0] for i in self.data]

    def __config_button(self):
        # Botón para realizar el descuento y genera tabla
        btn_ok = tk.Button(self.root, text = "Aplicar", command = self.procedimiento)
        btn_ok.place(x = 50, y = 160, width = 80, height = 20)

        # Cancelar la consulta
        btn_cancel = tk.Button(self.root, text = "Cancelar", command = self.root.destroy)
        btn_cancel.place(x = 170, y = 160, width = 80, height = 20)

    def procedimiento(self):
        sql = """select cuanto_editorial(%(id)s);"""
        self.cont = self.libreria.run_select_filter(sql, {"id": self.ids[self.combo.current()]})[0]
        self.entry_user = ttk.Label(self.root, text=self.cont)
        self.entry_user.place(x = 170, y = 90, width = 140, height = 30)
