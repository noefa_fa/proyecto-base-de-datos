import tkinter as tk
from tkinter import ttk
from tkcalendar import DateEntry

from cliente import cliente
from empleado import empleado
from tkinter import messagebox
from tkinter import messagebox


class compra:
    def __init__(self, root, db):
        self.libreria = db
        self.data = []

        #Toplevel es una ventana que está un nivel arriba que la principal
        self.root = tk.Toplevel()
        self.root.geometry('750x400')
        self.root.title("Compra")
        self.root.resizable(width=0, height=0)

        # toplevel modal
        self.root.transient(root)

        #
        self.__config_treeview_compra()
        self.__config_buttons_compra()

    def __config_treeview_compra(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1", "#2", "#3", "#4"))
        self.treeview.heading("#0", text = "Id")
        self.treeview.heading("#1", text = "Fecha")
        self.treeview.heading("#2", text = "Total")
        self.treeview.heading("#3", text = "Cliente")
        self.treeview.heading("#4", text = "Empleado")
        self.treeview.column("#0", minwidth = 100, width = 120, stretch = False)
        self.treeview.column("#1", minwidth = 160, width = 160, stretch = False)
        self.treeview.column("#2", minwidth = 160, width = 160, stretch = False)
        self.treeview.column("#3", minwidth = 160, width = 160, stretch = False)
        self.treeview.column("#4", minwidth = 160, width = 160, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 350, width = 750)
        self.llenar_treeview_compra()
        self.root.after(0, self.llenar_treeview_compra)

    def __config_buttons_compra(self):
        tk.Button(self.root, text="Insertar compra",
            command = self.insertar_compra).place(x = 0, y = 350, width = 250, height = 50)
        tk.Button(self.root, text="Modificar compra",
            command = self.modificar_compra).place(x = 250, y = 350, width = 250, height = 50)
        tk.Button(self.root, text="Eliminar compra",
            command = self.eliminar_compra).place(x = 500, y = 350, width = 250, height = 50)

    def llenar_treeview_compra(self):
        sql = """select idCompra, fecha, total_pago, concat(Cliente.nombre, " ", Cliente.apellido),
                concat(Empleado.nombre, " ", Empleado.apellido)
                from Compra join Cliente join Empleado
                on Cliente.idCliente = Compra.Cliente_idCliente
                and Compra.Empleado_idEmpleado = Empleado.idEmpleado"""
        # Ejecuta el select
        data = self.libreria.run_select(sql)

        # Si la data es distina a la que hay actualmente...
        if(data != self.data):
            # Elimina todos los rows del treeview
            self.treeview.delete(*self.treeview.get_children())#Elimina todos los rows del treeview
            for i in data:
                #Inserta los datos
                self.treeview.insert("", "end", text = i[0],
                    values = (i[1], i[2], i[3], i[4]), iid = i[0])
            self.data = data  #Actualiza la data


    def insertar_compra(self):
        insertar_compra(self.libreria, self)

    def modificar_compra(self):
        if(self.treeview.focus() != ""):
            if messagebox.askyesno(message="¿Realmente quieres modificar este registro?", title = "Alerta") == True:
                sql = """select idCompra, fecha, total_pago, concat(Cliente.nombre, " ", Cliente.apellido),
                        concat(Empleado.nombre, " ", Empleado.apellido) from Compra join Cliente join Empleado
                        on Cliente.idCliente = Compra.Cliente_idCliente and Compra.Empleado_idEmpleado = Empleado.idEmpleado
                        where idCompra = %(idCompra)s"""
                row_data = self.libreria.run_select_filter(sql, {"idCompra": self.treeview.focus()})[0]
                modificar_compra(self.libreria, self, row_data)

    def eliminar_compra(self):
        if(self.treeview.focus() != ""):
            if messagebox.askyesno(message="¿Realmente quieres eliminar este registro?", title = "Alerta") == True:
                sql = "delete from Compra where idCompra = %(idCompra)s"
                self.libreria.run_sql(sql, {"idCompra": self.treeview.focus()})
                self.llenar_treeview_compra()


class insertar_compra:
    def __init__(self, db, padre):
        self.padre = padre
        self.libreria = db
        self.insert_datos = tk.Toplevel()
        self.__config_window()
        self.__config_label()
        self.__config_entry()
        self.__config_button()

    def __config_window(self):
        self.insert_datos.geometry('300x200')
        self.insert_datos.title("Insertar compra")
        self.insert_datos.resizable(width=0, height=0)

    def __config_label(self):
        tk.Label(self.insert_datos, text = "Fecha: ").place(x = 10, y = 10, width = 80, height = 30)
        tk.Label(self.insert_datos, text = "Total: ").place(x = 10, y = 50, width = 80, height = 30)
        tk.Label(self.insert_datos, text = "Cliente: ").place(x = 10, y = 90, width = 80, height = 30)
        tk.Label(self.insert_datos, text = "Empleado: ").place(x = 10, y = 130, width = 80, height = 30)


    def __config_entry(self):
        self.combo1 = DateEntry(self.insert_datos,selectmode='day')
        self.combo1.place(x = 100, y = 10, width = 180, height= 30)
        self.entry_total = tk.Entry(self.insert_datos)
        self.entry_total.place(x = 100, y = 50, width = 180, height = 30)
        self.combo_cliente = ttk.Combobox(self.insert_datos)
        self.combo_cliente.place(x = 100, y = 90, width = 180, height= 30)
        self.combo_cliente["values"], self.idsg = self.__fill_combo_cliente()
        self.combo_empleado= ttk.Combobox(self.insert_datos)
        self.combo_empleado.place(x = 100, y = 130, width = 180, height= 30)
        self.combo_empleado["values"], self.idse = self.__fill_combo_empleado()

    def __config_button(self):
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.__insertar).place(x=0, y =170, width = 300, height = 30)

    def __fill_combo_cliente(self):
        sql = """select idCliente, concat(nombre, " ", apellido) from Cliente order by idCliente asc"""
        self.data = self.libreria.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]

    def __fill_combo_empleado(self):
        sql = """select idEmpleado, concat(nombre, " ", apellido) from Empleado order by idEmpleado asc"""
        self.data = self.libreria.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]


    def __insertar(self): #Insercion en la base de datos.
        sql = """insert into Compra
                (fecha, total_pago, Cliente_idCliente, Empleado_idEmpleado)
                values (%(fecha)s, %(total_pago)s, %(Cliente_idCliente)s, %(Empleado_idEmpleado)s)"""
        self.libreria.run_sql(sql, {"fecha": self.combo1.get_date(),
            "total_pago": self.entry_total.get(),
            "Cliente_idCliente": self.idsg[self.combo_cliente.current()],
            "Empleado_idEmpleado": self.idse[self.combo_empleado.current()]})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_compra()

class modificar_compra:
    def __init__(self, db, padre, row_data):
        self.padre = padre
        self.libreria = db
        self.row_data = row_data
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self): #Settings
        self.insert_datos.geometry('300x200')
        self.insert_datos.title("Modificar compra")
        self.insert_datos.resizable(width=0, height=0)

    def config_label(self):
        tk.Label(self.insert_datos, text = "Fecha: ").place(x = 10, y = 10, width = 80, height = 30)
        tk.Label(self.insert_datos, text = "Total: ").place(x = 10, y = 50, width = 80, height = 30)
        tk.Label(self.insert_datos, text = "Cliente: ").place(x = 10, y = 90, width = 80, height = 30)
        tk.Label(self.insert_datos, text = "Empleado: ").place(x = 10, y = 130, width = 80, height = 30)


    def config_entry(self):
        self.combo1 = DateEntry(self.insert_datos,selectmode='day')
        self.combo1.place(x = 100, y = 10, width = 180, height= 30)
        self.entry_total = tk.Entry(self.insert_datos)
        self.entry_total.place(x = 100, y = 50, width = 180, height = 30)
        self.combo_cliente = ttk.Combobox(self.insert_datos)
        self.combo_cliente.place(x = 100, y = 90, width = 180, height= 30)
        self.combo_cliente["values"], self.idsg = self.__fill_combo_cliente()
        self.combo_empleado= ttk.Combobox(self.insert_datos)
        self.combo_empleado.place(x = 100, y = 130, width = 180, height= 30)
        self.combo_empleado["values"], self.idse = self.__fill_combo_empleado()
        self.combo1.insert(0, self.row_data[1])
        self.entry_total.insert(0, self.row_data[2])
        self.combo_cliente.insert(0, self.row_data[3])
        self.combo_empleado.insert(0, self.row_data[4])


    def config_button(self): #Botón aceptar, llama a la función modificar cuando es clickeado.
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.modificar).place(x=10, y =170, width = 300, height = 30)

    def __fill_combo_cliente(self):
        sql = """select idCliente, concat(nombre, " ", apellido) as cliente from Cliente order by idCliente asc"""
        self.data = self.libreria.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]

    def __fill_combo_empleado(self):
        sql = """select idEmpleado, concat(nombre, " ", apellido) as empleado from Empleado order by idEmpleado asc"""
        self.data = self.libreria.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]


    def modificar(self): #Insercion en la base de datos.
        sql = """update Compra set fecha = %(fecha)s, total_pago = %(total_pago)s,
            Cliente_idCliente = %(Cliente_idCliente)s, Empleado_idEmpleado = %(Empleado_idEmpleado)s
            where idCompra = %(idCompra)s"""
        self.libreria.run_sql(sql, {"fecha": self.combo1.get_date(),
            "total_pago": self.entry_total.get(),
            "Cliente_idCliente": self.idsg[self.combo_cliente.current()],
            "Empleado_idEmpleado": self.idse[self.combo_empleado.current()],
            "idCompra": int(self.row_data[0])})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_compra()
