-- MySQL dump 10.13  Distrib 8.0.27, for Linux (x86_64)
--
-- Host: localhost    Database: libreria
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Autor`
--

DROP TABLE IF EXISTS `Autor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Autor` (
  `idAutor` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `apellido` varchar(45) DEFAULT NULL,
  `pseudonimo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idAutor`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Autor`
--

LOCK TABLES `Autor` WRITE;
/*!40000 ALTER TABLE `Autor` DISABLE KEYS */;
INSERT INTO `Autor` VALUES (5,'Alice','Kellen',''),(6,'Benjamin','Sáenz',''),(7,'','','J.K. Rowling'),(22,'Medeline','Miller',''),(23,'Rainbow','Rowell',''),(24,'Elisabet ','Benavent','');
/*!40000 ALTER TABLE `Autor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Cliente`
--

DROP TABLE IF EXISTS `Cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Cliente` (
  `idCliente` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `telefono` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idCliente`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Cliente`
--

LOCK TABLES `Cliente` WRITE;
/*!40000 ALTER TABLE `Cliente` DISABLE KEYS */;
INSERT INTO `Cliente` VALUES (2,'Josefa','Hillmer','93274815'),(3,'Rodrigo ','Valenzuela',''),(4,'Franco','Cifuentes',''),(5,'Luis','Rebolledo','9673484');
/*!40000 ALTER TABLE `Cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Compra`
--

DROP TABLE IF EXISTS `Compra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Compra` (
  `idCompra` int NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `total_pago` int NOT NULL,
  `Cliente_idCliente` int NOT NULL,
  `Empleado_idEmpleado` int NOT NULL,
  PRIMARY KEY (`idCompra`),
  KEY `fk_Compra_Cliente1_idx` (`Cliente_idCliente`),
  KEY `fk_Compra_Empleado1_idx` (`Empleado_idEmpleado`),
  CONSTRAINT `fk_Compra_Cliente1` FOREIGN KEY (`Cliente_idCliente`) REFERENCES `Cliente` (`idCliente`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_Compra_Empleado1` FOREIGN KEY (`Empleado_idEmpleado`) REFERENCES `Empleado` (`idEmpleado`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Compra`
--

LOCK TABLES `Compra` WRITE;
/*!40000 ALTER TABLE `Compra` DISABLE KEYS */;
INSERT INTO `Compra` VALUES (1,'2021-12-09',16000,2,3),(2,'2021-12-09',30000,3,4),(3,'2021-12-10',20000,2,3),(4,'2021-12-07',28000,3,3),(5,'2021-12-10',30000,4,5);
/*!40000 ALTER TABLE `Compra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Editorial`
--

DROP TABLE IF EXISTS `Editorial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Editorial` (
  `idEditorial` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`idEditorial`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Editorial`
--

LOCK TABLES `Editorial` WRITE;
/*!40000 ALTER TABLE `Editorial` DISABLE KEYS */;
INSERT INTO `Editorial` VALUES (2,'Destino'),(3,'Planeta'),(4,'Salamandra'),(5,'AdN'),(7,'Vyra'),(8,'Booket');
/*!40000 ALTER TABLE `Editorial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Ejemplar`
--

DROP TABLE IF EXISTS `Ejemplar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Ejemplar` (
  `idEjemplares` int NOT NULL AUTO_INCREMENT,
  `Libro_idLibro` int NOT NULL,
  PRIMARY KEY (`idEjemplares`),
  KEY `fk_Ejemplares_Libro1_idx` (`Libro_idLibro`),
  CONSTRAINT `fk_Ejemplares_Libro1` FOREIGN KEY (`Libro_idLibro`) REFERENCES `Libro` (`idLibro`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Ejemplar`
--

LOCK TABLES `Ejemplar` WRITE;
/*!40000 ALTER TABLE `Ejemplar` DISABLE KEYS */;
INSERT INTO `Ejemplar` VALUES (1,2),(2,7),(3,11);
/*!40000 ALTER TABLE `Ejemplar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Ejemplar_participa_compra`
--

DROP TABLE IF EXISTS `Ejemplar_participa_compra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Ejemplar_participa_compra` (
  `Compra_idCompra` int NOT NULL,
  `Ejemplar_idEjemplares` int NOT NULL,
  `precio_venta` int NOT NULL,
  PRIMARY KEY (`Compra_idCompra`,`Ejemplar_idEjemplares`),
  KEY `fk_Ejemplar_participa_compra_Ejemplar1_idx` (`Ejemplar_idEjemplares`),
  CONSTRAINT `fk_Ejemplar_participa_compra_Compra1` FOREIGN KEY (`Compra_idCompra`) REFERENCES `Compra` (`idCompra`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_Ejemplar_participa_compra_Ejemplar1` FOREIGN KEY (`Ejemplar_idEjemplares`) REFERENCES `Ejemplar` (`idEjemplares`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Ejemplar_participa_compra`
--

LOCK TABLES `Ejemplar_participa_compra` WRITE;
/*!40000 ALTER TABLE `Ejemplar_participa_compra` DISABLE KEYS */;
INSERT INTO `Ejemplar_participa_compra` VALUES (1,1,12000),(2,1,20000),(3,2,50000),(5,3,15000);
/*!40000 ALTER TABLE `Ejemplar_participa_compra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Empleado`
--

DROP TABLE IF EXISTS `Empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Empleado` (
  `idEmpleado` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `telefono` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idEmpleado`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Empleado`
--

LOCK TABLES `Empleado` WRITE;
/*!40000 ALTER TABLE `Empleado` DISABLE KEYS */;
INSERT INTO `Empleado` VALUES (3,'Noemí','Gómez','983628472'),(4,'Paola','Alvarez',''),(5,'Emma','Sepulveda',''),(6,'Dante','Aguirre',''),(7,'Andres','Quezada',''),(19,'Catalina','Yañez','');
/*!40000 ALTER TABLE `Empleado` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `trigger_llegada_empleados` AFTER INSERT ON `Empleado` FOR EACH ROW begin insert into llegada_empleados
                (idEmpleado, nombre, apellido, fecha_llegada) values
                (NEW.idEmpleado, NEW.nombre, NEW.apellido, current_date); end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `Genero`
--

DROP TABLE IF EXISTS `Genero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Genero` (
  `idGenero` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`idGenero`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Genero`
--

LOCK TABLES `Genero` WRITE;
/*!40000 ALTER TABLE `Genero` DISABLE KEYS */;
INSERT INTO `Genero` VALUES (2,'Fantasia'),(4,'Comedia'),(5,'Romance');
/*!40000 ALTER TABLE `Genero` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Libro`
--

DROP TABLE IF EXISTS `Libro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Libro` (
  `idLibro` int NOT NULL AUTO_INCREMENT,
  `Genero_idGenero` int NOT NULL,
  `Editorial_idEditorial` int NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `precio` int NOT NULL,
  PRIMARY KEY (`idLibro`),
  KEY `fk_Libro_Genero1_idx` (`Genero_idGenero`),
  KEY `fk_Libro_Editorial1_idx` (`Editorial_idEditorial`),
  CONSTRAINT `fk_Libro_Editorial1` FOREIGN KEY (`Editorial_idEditorial`) REFERENCES `Editorial` (`idEditorial`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_Libro_Genero1` FOREIGN KEY (`Genero_idGenero`) REFERENCES `Genero` (`idGenero`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Libro`
--

LOCK TABLES `Libro` WRITE;
/*!40000 ALTER TABLE `Libro` DISABLE KEYS */;
INSERT INTO `Libro` VALUES (2,5,3,'Nosotros en la luna',15000),(7,2,5,'Harry Potter y la cámara secreta',6000),(8,4,8,'Desembarazada',13500),(11,5,8,'Carry On',12000),(12,4,3,'Un cuento perfecto',13500),(13,2,5,'Circe',19000),(14,2,4,'Harry Potter y la piedra filosofal',5500),(15,5,5,'Aristóteles y Dante descubren los secretos del universo',13000),(16,5,3,'Todo lo que nunca fuimos',13000);
/*!40000 ALTER TABLE `Libro` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `trigger_cambio_precio` AFTER UPDATE ON `Libro` FOR EACH ROW begin insert into cambio_precio (idLibro,
                    titulo, precio, precio_cambio, fecha) values (NEW.idLibro,
                    NEW.titulo, OLD.precio, NEW.precio, current_date); end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `Libro_has_Autor`
--

DROP TABLE IF EXISTS `Libro_has_Autor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Libro_has_Autor` (
  `Libro_idLibro` int NOT NULL,
  `Autor_idAutor` int NOT NULL,
  PRIMARY KEY (`Libro_idLibro`,`Autor_idAutor`),
  KEY `fk_Libro_has_Autor_Autor1_idx` (`Autor_idAutor`),
  KEY `fk_Libro_has_Autor_Libro1_idx` (`Libro_idLibro`),
  CONSTRAINT `fk_Libro_has_Autor_Autor1` FOREIGN KEY (`Autor_idAutor`) REFERENCES `Autor` (`idAutor`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Libro_has_Autor_Libro1` FOREIGN KEY (`Libro_idLibro`) REFERENCES `Libro` (`idLibro`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Libro_has_Autor`
--

LOCK TABLES `Libro_has_Autor` WRITE;
/*!40000 ALTER TABLE `Libro_has_Autor` DISABLE KEYS */;
INSERT INTO `Libro_has_Autor` VALUES (2,5),(16,5),(15,6),(7,7),(14,7),(13,22),(11,23);
/*!40000 ALTER TABLE `Libro_has_Autor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cambio_precio`
--

DROP TABLE IF EXISTS `cambio_precio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cambio_precio` (
  `id_cambio` int NOT NULL AUTO_INCREMENT,
  `idLibro` int DEFAULT NULL,
  `titulo` varchar(200) DEFAULT NULL,
  `precio` int DEFAULT NULL,
  `precio_cambio` int DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  PRIMARY KEY (`id_cambio`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cambio_precio`
--

LOCK TABLES `cambio_precio` WRITE;
/*!40000 ALTER TABLE `cambio_precio` DISABLE KEYS */;
INSERT INTO `cambio_precio` VALUES (6,17,'Heartstopper 1',8900,7000,'2021-12-12'),(7,8,'Desembarazada',15000,13500,'2021-12-13'),(8,12,'Un cuento perfecto',15000,13500,'2021-12-13');
/*!40000 ALTER TABLE `cambio_precio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `llegada_empleados`
--

DROP TABLE IF EXISTS `llegada_empleados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `llegada_empleados` (
  `id_llegada` int NOT NULL AUTO_INCREMENT,
  `idEmpleado` int DEFAULT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `apellido` varchar(50) DEFAULT NULL,
  `fecha_llegada` date DEFAULT NULL,
  PRIMARY KEY (`id_llegada`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `llegada_empleados`
--

LOCK TABLES `llegada_empleados` WRITE;
/*!40000 ALTER TABLE `llegada_empleados` DISABLE KEYS */;
INSERT INTO `llegada_empleados` VALUES (6,19,'Catalina','Yañez','2021-12-12');
/*!40000 ALTER TABLE `llegada_empleados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `view_autor`
--

DROP TABLE IF EXISTS `view_autor`;
/*!50001 DROP VIEW IF EXISTS `view_autor`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `view_autor` AS SELECT 
 1 AS `Titulo`,
 1 AS `Genero`,
 1 AS `Editorial`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `view_empleado`
--

DROP TABLE IF EXISTS `view_empleado`;
/*!50001 DROP VIEW IF EXISTS `view_empleado`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `view_empleado` AS SELECT 
 1 AS `titulo`,
 1 AS `fecha`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping routines for database 'libreria'
--
/*!50003 DROP FUNCTION IF EXISTS `cuanto_editorial` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `cuanto_editorial`(editorial int) RETURNS int
    DETERMINISTIC
begin return (select count(idLibro) from Libro where Editorial_idEditorial = editorial); end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `aplicar_descuento` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `aplicar_descuento`(porcentaje float, genero int)
begin
                update Libro set precio = precio * (1 - porcentaje) where Genero_idGenero = genero;
                end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `view_autor`
--

/*!50001 DROP VIEW IF EXISTS `view_autor`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`libreria`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_autor` AS select `Libro`.`titulo` AS `Titulo`,`Genero`.`nombre` AS `Genero`,`Editorial`.`nombre` AS `Editorial` from ((((`Libro` join `Autor`) join `Libro_has_Autor`) join `Genero`) join `Editorial` on(((`Libro`.`idLibro` = `Libro_has_Autor`.`Libro_idLibro`) and (`Libro_has_Autor`.`Autor_idAutor` = `Autor`.`idAutor`) and (`Libro`.`Genero_idGenero` = `Genero`.`idGenero`) and (`Libro`.`Editorial_idEditorial` = `Editorial`.`idEditorial`)))) where (`Autor`.`idAutor` = 5) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_empleado`
--

/*!50001 DROP VIEW IF EXISTS `view_empleado`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`libreria`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_empleado` AS select `Libro`.`titulo` AS `titulo`,`Compra`.`fecha` AS `fecha` from ((((`Empleado` join `Compra`) join `Ejemplar_participa_compra`) join `Ejemplar`) join `Libro` on(((`Empleado`.`idEmpleado` = `Compra`.`Empleado_idEmpleado`) and (`Compra`.`idCompra` = `Ejemplar_participa_compra`.`Compra_idCompra`) and (`Ejemplar_participa_compra`.`Ejemplar_idEjemplares` = `Ejemplar`.`idEjemplares`) and (`Ejemplar`.`Libro_idLibro` = `Libro`.`idLibro`)))) where (`Empleado`.`idEmpleado` = 4) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-13 17:28:16
