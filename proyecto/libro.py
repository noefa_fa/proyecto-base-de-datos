import tkinter as tk
from tkinter import ttk

from genero import genero
from editorial import editorial
from tkinter import messagebox

class libro:
    def __init__(self, root, db):
        self.libreria = db
        self.data = []

        #Toplevel es una ventana que está un nivel arriba que la principal
        self.root = tk.Toplevel()
        self.root.geometry('750x400')
        self.root.title("Libros")
        self.root.resizable(width=0, height=0)

        # toplevel modal
        self.root.transient(root)

        #
        self.__config_treeview_libro()
        self.__config_buttons_libro()

    def __config_treeview_libro(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1", "#2", "#3", "#4"))
        self.treeview.heading("#0", text = "Id")
        self.treeview.heading("#1", text = "Título")
        self.treeview.heading("#2", text = "Precio")
        self.treeview.heading("#3", text = "Género")
        self.treeview.heading("#4", text = "Editorial")
        self.treeview.column("#0", minwidth = 100, width = 120, stretch = False)
        self.treeview.column("#1", minwidth = 260, width = 260, stretch = False)
        self.treeview.column("#2", minwidth = 160, width = 160, stretch = False)
        self.treeview.column("#3", minwidth = 110, width = 110, stretch = False)
        self.treeview.column("#4", minwidth = 110, width = 110, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 350, width = 750)
        self.llenar_treeview_libro()
        self.root.after(0, self.llenar_treeview_libro)

    def __config_buttons_libro(self):
        tk.Button(self.root, text="Insertar libro",
            command = self.__insertar_libro).place(x = 0, y = 350, width = 250, height = 50)
        tk.Button(self.root, text="Modificar libro",
            command = self.__modificar_libro).place(x = 250, y = 350, width = 250, height = 50)
        tk.Button(self.root, text="Eliminar libro",
            command = self.__eliminar_libro).place(x = 500, y = 350, width = 250, height = 50)

    def llenar_treeview_libro(self):
        sql = """select idLibro, titulo, precio, Genero.nombre, Editorial.nombre
        from Libro join Genero join Editorial on Genero.idGenero = Libro.Genero_idGenero
        and Editorial.idEditorial = Libro.Editorial_idEditorial;"""

        # Ejecuta el select
        data = self.libreria.run_select(sql)

        # Si la data es distina a la que hay actualmente...
        if(data != self.data):
            # Elimina todos los rows del treeview
            self.treeview.delete(*self.treeview.get_children())#Elimina todos los rows del treeview
            for i in data:
                #Inserta los datos
                self.treeview.insert("", "end", text = i[0],
                    values = (i[1], i[2], i[3], i[4]), iid = i[0])
            self.data = data  #Actualiza la data


    def __insertar_libro(self):
        insertar_libro(self.libreria, self)


    def __modificar_libro(self):
        if(self.treeview.focus() != ""):
            if messagebox.askyesno(message="¿Realmente quieres modificar este registro?", title = "Alerta") == True:
                sql = """select idLibro, titulo, precio, Genero.nombre,
                    Editorial.nombre from Libro join Genero join Editorial
                    on Genero.idGenero = Libro.Genero_idGenero and
                    Editorial.idEditorial = Libro.Editorial_idEditorial
                    where idLibro = %(idLibro)s"""
                row_data = self.libreria.run_select_filter(sql, {"idLibro": self.treeview.focus()})[0]
                modificar_libro(self.libreria, self, row_data)

    def __eliminar_libro(self):
            if(self.treeview.focus() != ""):
                if messagebox.askyesno(message="¿Realmente quieres eliminar este registro?", title = "Alerta") == True:
                    sql = "delete from Libro where idLibro = %(idLibro)s"
                    self.libreria.run_sql(sql, {"idLibro": self.treeview.focus()})
                    self.llenar_treeview_libro()


class insertar_libro:
    def __init__(self, db, padre):
        self.padre = padre
        self.libreria = db
        self.insert_datos = tk.Toplevel()
        self.__config_window()
        self.__config_label()
        self.__config_entry()
        self.__config_button()

    def __config_window(self):
        self.insert_datos.geometry('300x200')
        self.insert_datos.title("Insertar libro")
        self.insert_datos.resizable(width=0, height=0)

    def __config_label(self):
        tk.Label(self.insert_datos, text = "Título: ").place(x = 10, y = 10, width = 80, height = 30)
        tk.Label(self.insert_datos, text = "Precio: ").place(x = 10, y = 50, width = 80, height = 30)
        tk.Label(self.insert_datos, text = "Género: ").place(x = 10, y = 90, width = 80, height = 30)
        tk.Label(self.insert_datos, text = "Editorial: ").place(x = 10, y = 130, width = 80, height = 30)

    def __config_entry(self):
        self.entry_titulo = tk.Entry(self.insert_datos)
        self.entry_titulo.place(x = 100, y = 10, width = 180, height = 30)
        self.entry_precio = tk.Entry(self.insert_datos)
        self.entry_precio.place(x = 100, y = 50, width = 180, height = 30)
        self.combo_genero = ttk.Combobox(self.insert_datos)
        self.combo_genero.place(x = 100, y = 90, width = 180, height= 30)
        self.combo_genero["values"], self.idsg = self.__fill_combo_genero()
        self.combo_editorial= ttk.Combobox(self.insert_datos)
        self.combo_editorial.place(x = 100, y = 130, width = 180, height= 30)
        self.combo_editorial["values"], self.idse = self.__fill_combo_editorial()

    def __config_button(self):
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.__insertar).place(x=0, y =170, width = 300, height = 30)

    def __fill_combo_genero(self):
        sql = "select idGenero, nombre from Genero order by idGenero asc"
        self.data = self.libreria.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]

    def __fill_combo_editorial(self):
        sql = "select idEditorial, nombre from Editorial order by idEditorial asc"
        self.data = self.libreria.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]

    def __insertar(self): #Insercion en la base de datos.
        sql = """insert into Libro
                (Genero_idGenero, Editorial_idEditorial, titulo, precio)
                values
                (%(Genero_idGenero)s, %(Editorial_idEditorial)s, %(titulo)s, %(precio)s)
                """
        #print(self.ids[self.combo_genero.current()])
        self.libreria.run_sql(sql, {"Genero_idGenero": self.idsg[self.combo_genero.current()],
                    "Editorial_idEditorial": self.idse[self.combo_editorial.current()],
                    "titulo": self.entry_titulo.get(),
                    "precio": self.entry_precio.get()})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_libro()

class modificar_libro:
    def __init__(self, db, padre, row_data):
        self.padre = padre
        self.libreria = db
        self.row_data = row_data
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self): #Settings
        self.insert_datos.geometry('300x200')
        self.insert_datos.title("Modificar libro")
        self.insert_datos.resizable(width=0, height=0)

    def config_label(self):
        tk.Label(self.insert_datos, text = "Título: ").place(x = 10, y = 10, width = 80, height = 30)
        tk.Label(self.insert_datos, text = "Precio: ").place(x = 10, y = 50, width = 80, height = 30)
        tk.Label(self.insert_datos, text = "Género: ").place(x = 10, y = 90, width = 80, height = 30)
        tk.Label(self.insert_datos, text = "Editorial: ").place(x = 10, y = 130, width = 80, height = 30)

    def config_entry(self):
        self.entry_titulo = tk.Entry(self.insert_datos)
        self.entry_titulo.place(x = 100, y = 10, width = 180, height = 30)
        self.entry_precio = tk.Entry(self.insert_datos)
        self.entry_precio.place(x = 100, y = 50, width = 180, height = 30)
        self.combo_genero = ttk.Combobox(self.insert_datos)
        self.combo_genero.place(x = 100, y = 90, width = 180, height= 30)
        self.combo_genero["values"], self.idsg = self.__fill_combo_genero()
        self.combo_editorial= ttk.Combobox(self.insert_datos)
        self.combo_editorial.place(x = 100, y = 130, width = 180, height= 30)
        self.combo_editorial["values"], self.idse = self.__fill_combo_editorial()
        self.entry_titulo.insert(0, self.row_data[1])
        self.entry_precio.insert(0, self.row_data[2])
        self.combo_genero.insert(0, self.row_data[3])
        self.combo_editorial.insert(0, self.row_data[3])


    def config_button(self):
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.__modificar).place(x=0, y =170, width = 300, height = 30)

    def __fill_combo_genero(self):
        sql = "select idGenero, nombre from Genero order by idGenero asc"
        self.data = self.libreria.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]

    def __fill_combo_editorial(self):
        sql = "select idEditorial, nombre from Editorial order by idEditorial asc"
        self.data = self.libreria.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]

    def __modificar(self): #Insercion en la base de datos.
        sql = """ update Libro set Genero_idGenero = %(Genero_idGenero)s,
                Editorial_idEditorial = %(Editorial_idEditorial)s,
                titulo = %(titulo)s, precio = %(precio)s
                where idLibro = %(idLibro)s"""
        self.libreria.run_sql(sql, {
                        "Genero_idGenero": self.idsg[self.combo_genero.current()],
                        "Editorial_idEditorial": self.idse[self.combo_editorial.current()],
                        "titulo": self.entry_titulo.get(),
                        "precio": self.entry_precio.get(),
                        "idLibro": int(self.row_data[0])})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_libro()
