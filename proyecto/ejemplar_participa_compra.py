import tkinter as tk
from tkinter import ttk
from tkinter import messagebox



class ejemplar_compra:
    def __init__(self, root, db):
        self.libreria = db
        self.data = []

        #Toplevel es una ventana que está un nivel arriba que la principal
        self.root = tk.Toplevel()
        self.root.geometry('750x400')
        self.root.title("Detalle venta")
        self.root.resizable(width=0, height=0)

        # toplevel modal
        self.root.transient(root)

        #
        self.__config_treeview_detalleventa()
        self.__config_buttons_detalleventa()

    def __config_treeview_detalleventa(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#0", "#1", "#2", "#3"))
        self.treeview.heading("#0", text = "Id Compra")
        self.treeview.heading("#1", text = "Id Ejemplar")
        self.treeview.heading("#2", text = "Precio Vendido")
        self.treeview.heading("#3", text = "Libro vendido")
        self.treeview.column("#0", minwidth = 100, width = 120, stretch = False)
        self.treeview.column("#1", minwidth = 220, width = 220, stretch = False)
        self.treeview.column("#2", minwidth = 220, width = 120, stretch = False)
        self.treeview.column("#3", minwidth = 320, width = 320, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 350, width = 750)
        self.llenar_treeview_detalleventa()
        self.root.after(0, self.llenar_treeview_detalleventa)


    def __config_buttons_detalleventa(self):
        tk.Button(self.root, text="Insertar venta",
            command = self.__insertar_detalleventa).place(x = 0, y = 350, width = 250, height = 50)
        tk.Button(self.root, text="Modificar venta",
            command = self.__modificar_detalleventa).place(x = 250, y = 350, width = 250, height = 50)
        tk.Button(self.root, text="Eliminar venta",
            command = self.__eliminar_detalleventa).place(x = 500, y = 350, width = 250, height = 50)
        #tk.Button(self.root, text="Ver autor(es)",
        #    command = self.__ver_autores).place(x = 750, y = 350, width = 100, height = 50)

    def llenar_treeview_detalleventa(self):
        sql = """select Compra_idCompra, Ejemplar_idEjemplares, precio_venta,
                 Libro.titulo from Ejemplar_participa_compra join Libro join Ejemplar
                 on Ejemplar_participa_compra.Ejemplar_idEjemplares = Ejemplar.idEjemplares
                 and Ejemplar.Libro_idLibro = Libro.idLibro"""

        # Ejecuta el select
        data = self.libreria.run_select(sql)

        # Si la data es distina a la que hay actualmente...
        if(data != self.data):
            # Elimina todos los rows del treeview
            self.treeview.delete(*self.treeview.get_children())#Elimina todos los rows del treeview
            for i in data:
                #Inserta los datos
                self.treeview.insert("", "end", text = i[0],
                                    values = (i[1], i[2], i[3]))
                    #, iid = i[0])
            self.data = data  #Actualiza la data


    def __insertar_detalleventa(self):
        insertar_detalleventa(self.libreria, self)


    def __modificar_detalleventa(self):
        if(self.treeview.focus() != ""):
            # Lista que contiene las columanas de todo el registro seleccionado
            item1 = (self.treeview.item(self.treeview.focus()))["text"]
            item2 = (self.treeview.item(self.treeview.focus()))["values"]
            #print("ITEM: ", item1, item2)
            # Se obtiene las claves primarias
            self.claves = [item1, item2[0]]
        #    self.first = item1
            #print("CLAVES: ", self.claves)

            if messagebox.askyesno(message="¿Realmente quieres modificar este registro?", title = "Alerta") == True:
                sql = """select Compra_idCompra, Ejemplar_idEjemplares, precio_venta
                    from Ejemplar_participa_compra
                    where Compra_idCompra = %(Compra_idCompra)s and
                    Ejemplar_idEjemplares = %(Ejemplar_idEjemplares)s;
                    """
                row_data = self.libreria.run_select_filter(sql, {"Compra_idCompra": self.claves[0],
                                                                "Ejemplar_idEjemplares": self.claves[1]})[0]
                modificar_detalleventa(self.libreria, self, row_data)

    def __eliminar_detalleventa(self):
        if(self.treeview.focus() != ""):
            # Lista que contiene las columanas de todo el registro seleccionado
            item1 = (self.treeview.item(self.treeview.focus()))["text"]
            item2 = (self.treeview.item(self.treeview.focus()))["values"]
            print("ITEM: ", item1, item2)
            # Se obtiene las claves primarias
            self.claves = [item1, item2[0]]
        #    self.first = item1
            print("CLAVES: ", self.claves)

            if messagebox.askyesno(message="¿Realmente quieres eliminar este registro?", title = "Alerta") == True:
                sql = """delete from Ejemplar_participa_compra where Compra_idCompra = %(idCompra)s
                        and Ejemplar_idEjemplares = %(idEjemplar)s"""
                self.libreria.run_sql(sql, {"idCompra": self.claves[0], "idEjemplar": self.claves[1]})
                self.llenar_treeview_detalleventa()

class insertar_detalleventa:
    def __init__(self, db, padre):
        self.padre = padre
        self.libreria = db
        self.insert_datos = tk.Toplevel()
        self.__config_window()
        self.__config_label()
        self.__config_entry()
        self.__config_button()

    def __config_window(self):
        self.insert_datos.geometry('300x200')
        self.insert_datos.title("Insertar detalleventa")
        self.insert_datos.resizable(width=0, height=0)

    def __config_label(self):
        tk.Label(self.insert_datos, text = "Id Compra: ").place(x = 10, y = 10, width = 80, height = 30)
        tk.Label(self.insert_datos, text = "Id Ejemplar: ").place(x = 10, y = 50, width = 80, height = 30)
        tk.Label(self.insert_datos, text = "Precio venta: ").place(x = 10, y = 90, width = 80, height = 30)
        #tk.Label(self.insert_datos, text = "Editorial: ").place(x = 10, y = 130, width = 80, height = 30)

    def __config_entry(self):
        self.combo_compra = ttk.Combobox(self.insert_datos)
        self.combo_compra.place(x = 100, y = 10, width = 180, height= 30)
        self.combo_compra["values"], self.idsg = self.__fill_combo_compra()
        self.combo_ejemplar = ttk.Combobox(self.insert_datos)
        self.combo_ejemplar.place(x = 100, y = 50, width = 180, height= 30)
        self.combo_ejemplar["values"], self.idse = self.__fill_combo_ejemplar()
        self.entry_precio = tk.Entry(self.insert_datos)
        self.entry_precio.place(x = 100, y = 90, width = 180, height = 30)


    def __config_button(self):
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.__insertar).place(x=0, y =170, width = 300, height = 30)

    def __fill_combo_compra(self):
        sql = "select idCompra, fecha from Compra"
        self.data = self.libreria.run_select(sql)
        return [i[0] for i in self.data], [i[0] for i in self.data]

    def __fill_combo_ejemplar(self):
        sql = "select idEjemplares, Libro_idLibro from Ejemplar"
        self.data = self.libreria.run_select(sql)
        return [i[0] for i in self.data], [i[0] for i in self.data]

    def __insertar(self): #Insercion en la base de datos.
        sql = """insert into Ejemplar_participa_compra
                    (Compra_idCompra, Ejemplar_idEjemplares, precio_venta)
                    values
                    (%(idCompra)s, %(idEjemplar)s, %(precio_venta)s)"""

        #print(self.ids[self.combo_genero.current()])
        self.libreria.run_sql(sql, {"idCompra": self.idsg[self.combo_compra.current()],
                    "idEjemplar": self.idse[self.combo_ejemplar.current()],
                    "precio_venta": self.entry_precio.get()})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_detalleventa()

class modificar_detalleventa:
    def __init__(self, db, padre, row_data):
        self.padre = padre
        self.libreria = db
        self.row_data = row_data
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self): #Settings
        self.insert_datos.geometry('300x200')
        self.insert_datos.title("Modificar detalle venta")
        self.insert_datos.resizable(width=0, height=0)

    def config_label(self):
        tk.Label(self.insert_datos, text = "Precio venta: ").place(x = 10, y = 10, width = 90, height = 30)

    def config_entry(self):
        self.entry_precio = tk.Entry(self.insert_datos)
        self.entry_precio.place(x = 100, y = 10, width = 180, height = 30)
        #self.combo_compra.insert(0, self.row_data[0])
        #self.combo_ejemplar.insert(0, self.row_data[1])
        self.entry_precio.insert(0, self.row_data[2])

    def config_button(self):
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.__modificar).place(x=0, y =170, width = 300, height = 30)

    def __fill_combo_compra(self):
        sql = "select idCompra, fecha from Compra"
        self.data = self.libreria.run_select(sql)
        return [i[0] for i in self.data], [i[0] for i in self.data]

    def __fill_combo_ejemplar(self):
        sql = "select idEjemplares, Libro_idLibro from Ejemplar"
        self.data = self.libreria.run_select(sql)
        return [i[0] for i in self.data], [i[0] for i in self.data]

    def __modificar(self): #Insercion en la base de datos.
        sql = """ update Ejemplar_participa_compra
                  set precio_venta = %(precio_venta)s where
                  Compra_idCompra = %(Compra_idCompra)s and
                  Ejemplar_idEjemplares = %(Ejemplar_idEjemplares)s"""
        print(self.row_data[0])
        self.libreria.run_sql(sql, {"precio_venta": self.entry_precio.get(),
                                    "Compra_idCompra": int(self.row_data[0]),
                                    "Ejemplar_idEjemplares": int(self.row_data[1])})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_detalleventa()
