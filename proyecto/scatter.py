import tkinter as tk
from tkinter import ttk
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib.pyplot as plot
#from sklearn.datasets import load_iris

class scatter:
    def __init__(self, root, db):
        self.root = root
        self.libreria = db
        # Se define la ventana que mostrara el histograma
        self.graph = tk.Toplevel()
        self.graph.geometry('700x500')
        self.graph.title("Scatter")

        # se llama a la funcion que permite la configuracion de la grafica
        self.__config_grafica()

    # Se define la funcion de la configuracion de grafica
    def __config_grafica(self):
        # Figura que hara el plot
        fig, ax = plot.subplots()
        x, y = self.obtencion_datos()

        # Titulo
        plot.title("Scatter: Ganancia por fecha")
        #ax.title("Scatter cantidad de libros por generos")
        # Nombre del eje x
        plot.xticks(rotation=20)
        plot.xlabel("Fecha")
        # Nombre del eje y
        plot.ylabel("Total")
        # Color de las barras
        plot.scatter(x, y, alpha=0.8, color = "#9c3d03")
        canvas = tk.Canvas(self.graph, width=700, height=500, background="white")
        canvas = FigureCanvasTkAgg(fig, master = self.graph)
        canvas.draw()
        canvas.get_tk_widget().pack()



# Funcion que permite obtener los datos desde sql
    def obtencion_datos(self):
        sql = """select fecha, total_pago from Compra;"""

        data = self.libreria.run_select(sql)
        # el eje X
        x = [i[0] for i in data]
        # el eje Y
        y = [i[1] for i in data]
        return x, y
