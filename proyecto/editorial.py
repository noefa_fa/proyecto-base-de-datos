import tkinter as tk
from tkinter import ttk
from tkinter import messagebox

class editorial:
    def __init__(self, root, db):
        self.libreria = db
        self.data = []

        # Toplevel es una ventana que está un nivel arriba que la principal
        self.root = tk.Toplevel()
        self.root.geometry('750x400')
        self.root.title("Editorial")
        self.root.resizable(width=0, height=0)

        # toplevel modal
        self.root.transient(root)

        #
        self.__config_treeview_editorial()
        self.__config_buttons_editorial()

        #self.root.after(0, self.llenar_treeview_editorial)


    def __config_treeview_editorial(self):#Se configura el treeview
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1"))
        self.treeview.heading("#0", text = "Id")
        self.treeview.heading("#1", text = "Nombre")
        self.treeview.column("#0", minwidth = 200, width = 200, stretch = False)
        self.treeview.column("#1", minwidth = 350, width = 530, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 350, width = 750)
        self.llenar_treeview_editorial()
        self.root.after(0, self.llenar_treeview_editorial)

    def __config_buttons_editorial(self):#Botones de insertar, modificar y eliminar
        tk.Button(self.root, text="Insertar editorial",
            command = self.__insertar_editorial).place(x = 0, y = 350, width = 250, height = 50)
        tk.Button(self.root, text="Modificar editorial",
            command = self.__modificar_editorial).place(x = 250, y = 350, width = 250, height = 50)
        tk.Button(self.root, text="Eliminar editorial",
            command = self.__eliminar_editorial).place(x = 500, y = 350, width = 250, height = 50)


    def llenar_treeview_editorial(self):#Se llena el treeview de datos.
        sql = "select * from Editorial"
        # Ejecuta el select
        data = self.libreria.run_select(sql)

        # Si la data es distina a la que hay actualmente...
        if(data != self.data):
            # Elimina todos los rows del treeview
            self.treeview.delete(*self.treeview.get_children())
            for i in data:
                #Inserta los datos
                self.treeview.insert("", "end", text = i[0],
                                    values = (i[1]), iid = i[0])
                self.data = data#Actualiza la data

    def __insertar_editorial(self):
        insertar_editorial(self.libreria, self)


    def __modificar_editorial(self):
        if(self.treeview.focus() != ""):
            if messagebox.askyesno(message="¿Realmente quieres modificar este registro?", title = "Alerta") == True:
                sql = "select * from Editorial where idEditorial = %(idEditorial)s"
                row_data = self.libreria.run_select_filter(sql, {"idEditorial": self.treeview.focus()})[0]
                modificar_editorial(self.libreria, self, row_data)

    def __eliminar_editorial(self):
        if(self.treeview.focus() != ""):
            if messagebox.askyesno(message="¿Realmente quieres eliminar este registro?", title = "Alerta") == True:
                sql = "delete from Editorial where idEditorial = %(idEditorial)s"
                self.libreria.run_sql(sql, {"idEditorial": self.treeview.focus()})
                self.llenar_treeview_editorial()

class insertar_editorial:#Clase para insertar data
    def __init__(self, db, padre):
        self.padre = padre
        self.libreria = db
        self.insert_datos = tk.Toplevel()
        self.__config_window()
        self.__config_label()
        self.__config_entry()
        self.__config_button()

    def __config_window(self):#Settings
        self.insert_datos.geometry('300x200')
        self.insert_datos.title("Insertar editorial")
        self.insert_datos.resizable(width=0, height=0)

    def __config_label(self):#Labels
        #tk.Label(self.insert_datos, text =  "idEditorial: ").place(x = 10, y = 10, width = 80, height = 20)
        tk.Label(self.insert_datos, text =  "Nombre: ").place(x = 10, y = 10, width = 80, height = 30)


    def __config_entry(self):#Se configuran los inputs
        self.entry_nombre = tk.Entry(self.insert_datos)
        self.entry_nombre.place(x = 100, y = 10, width = 180, height = 30)

    def __config_button(self):#Se configura el boton
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.__insertar).place(x=0, y =170, width = 300, height = 30)

    def __insertar(self): #Insercion en la base de datos.
        sql = """insert into Editorial (nombre)
            values (%(nombre)s)"""
        self.libreria.run_sql(sql, {"nombre": self.entry_nombre.get()})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_editorial()


class modificar_editorial:#Clase para modificar
    def __init__(self, db, padre, row_data):
        self.padre = padre
        self.libreria = db
        self.row_data = row_data
        self.insert_datos = tk.Toplevel()
        self.__config_window()
        self.__config_label()
        self.__config_entry()
        self.__config_button()

    def __config_window(self):#Configuración de la ventana.
        self.insert_datos.geometry('300x200')
        self.insert_datos.title("Modificar editorial")
        self.insert_datos.resizable(width=0, height=0)

    def __config_label(self):#Se configuran las etiquetas.
    #    tk.Label(self.insert_datos, text =  "idEditorial: ").place(x = 10, y = 10, width = 80, height = 30)
        tk.Label(self.insert_datos, text =  "Nombre: ").place(x = 10, y = 50, width = 80, height = 30)

    def __config_entry(self):#Se configuran los inputs
        #self.entry_idEditorial = tk.Entry(self.insert_datos)
        #self.entry_idEditorial.place(x = 100, y = 10, width = 180, height = 20)
        self.entry_nombre = tk.Entry(self.insert_datos)
        self.entry_nombre.place(x = 100, y = 50, width = 180, height = 20)
        #self.entry_idEditorial.insert(0, self.row_data[0])
        self.entry_nombre.insert(0, self.row_data[1])

    def __config_button(self): #Botón aceptar, llama a la función modificar cuando es clickeado.
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.__modificar).place(x=0, y =170, width = 300, height = 30)

    def __modificar(self): #Insercion en la base de datos.
        sql = """update Editorial set nombre = %(nombre)s
                    where idEditorial = %(idEditorial)s"""
        self.libreria.run_sql(sql, {"nombre": self.entry_nombre.get(),
                #"idEditorialn": self.entry_idEditorial.get(),
                "idEditorial": int(self.row_data[0])})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_editorial()
