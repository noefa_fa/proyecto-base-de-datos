import tkinter as tk
from tkinter import ttk
from tkinter import messagebox

class ejemplar:
    def __init__(self, root, db):
        self.libreria = db
        self.data = []

        # Toplevel es una ventana que está un nivel arriba que la principal
        self.root = tk.Toplevel()
        self.root.geometry('750x400')
        self.root.title("Ejemplares")
        self.root.resizable(width=0, height=0)

        # toplevel modal
        self.root.transient(root)

        #
        self.__config_treeview_ejemplar()
        self.__config_buttons_ejemplar()


    def __config_treeview_ejemplar(self):#Se configura el treeview
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#0", "#1", "#2"))
        self.treeview.heading("#0", text = "idEjemplar")
        self.treeview.heading("#1", text = "idLibro")
        self.treeview.heading("#2", text = "Libro")
        self.treeview.column("#0", minwidth = 100, width = 150, stretch = False)
        self.treeview.column("#1", minwidth = 100, width = 150, stretch = False)
        self.treeview.column("#2", minwidth = 550, width = 550, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 350, width = 750)
        self.llenar_treeview_ejemplar()
        self.root.after(0, self.llenar_treeview_ejemplar)

    def __config_buttons_ejemplar(self):#Botones de insertar, modificar y eliminar
        tk.Button(self.root, text="Insertar ejemplar",
            command = self.__insertar_ejemplar).place(x = 0, y = 350, width = 250, height = 50)
        tk.Button(self.root, text="Modificar ejemplar",
            command = self.__modificar_ejemplar).place(x = 250, y = 350, width = 250, height = 50)
        tk.Button(self.root, text="Eliminar ejemplar",
            command = self.__eliminar_ejemplar).place(x = 500, y = 350, width = 250, height = 50)


    def llenar_treeview_ejemplar(self):#Se llena el treeview de datos.
        sql = """select idEjemplares, idLibro, Libro.titulo as Titulo from Ejemplar join Libro
                on Ejemplar.Libro_idLibro = Libro.idLibro"""
        # Ejecuta el select
        data = self.libreria.run_select(sql)

        # Si la data es distina a la que hay actualmente...
        if(data != self.data):
            # Elimina todos los rows del treeview
            self.treeview.delete(*self.treeview.get_children())
            for i in data:
                #Inserta los datos
                self.treeview.insert("", "end", text = i[0],
                                    values = (i[1], i[2]), iid = i[0])
                self.data = data#Actualiza la data

    def __insertar_ejemplar(self):
        insertar_ejemplar(self.libreria, self)


    def __modificar_ejemplar(self):
        if(self.treeview.focus() != ""):
            if messagebox.askyesno(message="¿Realmente quieres modificar este registro?", title = "Alerta") == True:
                sql = """select idEjemplares, Libro.titulo from Ejemplar join Libro
                        on Ejemplar.Libro_idLibro = Libro.idLibro where idEjemplares = %(idEjemplares)s"""
                row_data = self.libreria.run_select_filter(sql, {"idEjemplares": self.treeview.focus()})[0]
                modificar_ejemplar(self.libreria, self, row_data)

    def __eliminar_ejemplar(self):
        if(self.treeview.focus() != ""):
            if messagebox.askyesno(message="¿Realmente quieres eliminar este registro?", title = "Alerta") == True:
                sql = "delete from Ejemplar where idEjemplares = %(idEjemplares)s"
                self.libreria.run_sql(sql, {"idEjemplares": self.treeview.focus()})
                self.llenar_treeview_ejemplar()

class insertar_ejemplar:#Clase para insertar data
    def __init__(self, db, padre):
        self.padre = padre
        self.libreria = db
        self.insert_datos = tk.Toplevel()
        self.__config_window()
        self.__config_label()
        self.__config_entry()
        self.__config_button()

    def __config_window(self):#Settings
        self.insert_datos.geometry('300x200')
        self.insert_datos.title("Insertar ejemplar")
        self.insert_datos.resizable(width=0, height=0)

    def __config_label(self):#Labels
        tk.Label(self.insert_datos, text = "Libro: ").place(x = 5, y = 10, width = 180, height = 30)


    def __config_entry(self):#Se configuran los inputs
        self.combo= ttk.Combobox(self.insert_datos)
        self.combo.place(x = 100, y = 15, width = 180, height= 30)
        self.combo["values"], self.ids = self.__fill_combo()

    def __config_button(self):#Se configura el boton
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.__insertar).place(x=0, y =170, width = 300, height = 30)

    def __fill_combo(self):
        sql = "select idLibro, titulo from Libro order by idLibro asc"
        self.data = self.libreria.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]

    def __insertar(self): #Insercion en la base de datos.
        sql = """insert into Ejemplar (Libro_idLibro) values (%(Libro_idLibro)s)"""
        self.libreria.run_sql(sql, {"Libro_idLibro": self.ids[self.combo.current()]})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_ejemplar()


class modificar_ejemplar:#Clase para modificar
    def __init__(self, db, padre, row_data):
        self.padre = padre
        self.libreria = db
        self.row_data = row_data
        self.insert_datos = tk.Toplevel()
        self.__config_window()
        self.__config_label()
        self.__config_entry()
        self.__config_button()

    def __config_window(self):#Configuración de la ventana.
        self.insert_datos.geometry('300x200')
        self.insert_datos.title("Modificar ejemplar")
        self.insert_datos.resizable(width=0, height=0)

    def __config_label(self):#Se configuran las etiquetas.
    #    tk.Label(self.insert_datos, text =  "idEjemplares: ").place(x = 10, y = 10, width = 80, height = 30)
        tk.Label(self.insert_datos, text =  "Libro: ").place(x = 10, y = 10, width = 80, height = 30)

    def __config_entry(self):#Se configuran los inputs
        self.combo= ttk.Combobox(self.insert_datos)
        self.combo.place(x = 100, y = 10, width = 180, height= 30)
        self.combo["values"], self.ids = self.__fill_combo()
        self.combo.insert(0, self.row_data[1])

    def __config_button(self): #Botón aceptar, llama a la función modificar cuando es clickeado.
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.__modificar).place(x=0, y =170, width = 300, height = 30)

    def __modificar(self): #Insercion en la base de datos.
        sql = """update Ejemplar set Libro_idLibro = %(Libro_idLibro)s
                    where idEjemplares = %(idEjemplares)s"""
        self.libreria.run_sql(sql, {#"idEjemplares": self.entry_idEjemplares.get(),
                #"idLibron": self.entry_IdLibro.get(),
                "idEjemplares": int(self.row_data[0]),
                "Libro_idLibro": self.ids[self.combo.current()]})

        self.insert_datos.destroy()
        self.padre.llenar_treeview_ejemplar()

    def __fill_combo(self):
        sql = "select idLibro, titulo from Libro order by idLibro asc"
        self.data = self.libreria.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]
