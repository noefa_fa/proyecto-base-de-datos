import tkinter as tk
from tkinter import ttk
import matplotlib.pyplot as plot
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

# Se crea una clase de histograma
class histograma:
    def __init__(self, root, db):
        self.root = root
        self.libreria = db
        # Se define la ventana que mostrara el histograma
        self.graph = tk.Toplevel()
        self.graph.geometry('600x400')
        self.graph.title("Histograma")

        # se llama a la funcion que permite la configuracion de la grafica
        self.__config_grafica()

    # Se define la funcion de la configuracion de grafica
    def __config_grafica(self):
        # Figura que hara el plot
        figura_canva, ax = plot.subplots()
        x, y = self.obtencion_datos()
        # Titulo
        plot.title("Histograma: Cantidad de libros por géneros")
        # Nombre del eje X
        plot.xlabel("Géneros")
        # Nombre del eje Y
        plot.ylabel("Cantidad de libros")
        # Color de las barras
        plot.bar(x, y, color = "#9c3d03")
        canvas = tk.Canvas(self.graph, width=600, height=500, background="white")
        canvas = FigureCanvasTkAgg(figura_canva, master = self.graph)
        canvas.draw()
        canvas.get_tk_widget().pack()


# Funcion que permite obtener los datos desde sql
    def obtencion_datos(self):
        sql = """select Genero.nombre, count(idLibro) from Libro
                join Genero on Genero.idGenero = Libro.Genero_idGenero
                group by Genero.nombre;"""

        data = self.libreria.run_select(sql)
        # el eje X
        x = [i[0] for i in data]
        # el eje Y
        y = [i[1] for i in data]
        return x, y
