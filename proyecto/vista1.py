import tkinter as tk
from tkinter import ttk
from tkinter import Button


class vista1:
    def __init__(self, root, db):
        # Se actualiza atributo con la database
        self.libreria = db
        self.data = []

        # Se crea una nueva ventana superior a la principal
        self.root = tk.Toplevel()
        self.root.geometry('300x270')
        self.root.title("Lista de libros de autor")
        self.root.resizable(width = 0, height = 0)
        self.root.transient(root)

        self.__config_label()
        self.__config_entry()
        self.__config_button()

        self.root.mainloop()

    def __config_label(self):
        # Definición de entradas de texto
        autor_lab = tk.Label(self.root, text = "Autor: ")
        autor_lab.place(x = 10, y = 35, width = 140, height = 20)

    def __config_entry(self):
        self.combo = ttk.Combobox(self.root)
        self.combo.place(x = 110, y = 35, width = 150, height= 20)
        self.combo["values"], self.ids = self.combo_autor()

    def combo_autor(self):
        row_data = """select idAutor, concat(nombre, " ", apellido, " ", pseudonimo)
                    as name from Autor; """

        self.data = self.libreria.run_select(row_data)
        # Se muestran los titulos de los libros para seleccionarlos
        return [i[1] for i in self.data], [i[0] for i in self.data]

    def __config_button(self):
        # ver las editoriales
        btn_ok = tk.Button(self.root, text = "Ver view",
            command = self.vista)
        btn_ok.place(x = 100, y = 230, width = 80, height = 20)

        # cancelar
        btn_cancel = tk.Button(self.root, text = "Cancelar",
            command = self.root.destroy)
        btn_cancel.place(x = 210, y = 230, width = 80, height = 20)

    def vista(self):
        sql = """create or replace view view_autor as select Libro.titulo as Titulo,
        Genero.nombre as Genero, Editorial.nombre as Editorial from Libro
        join Autor join Libro_has_Autor join Genero join Editorial on
        Libro.idLibro = Libro_has_Autor.Libro_idLibro and
        Libro_has_Autor.Autor_idAutor = Autor.idAutor and
        Libro.Genero_idGenero = Genero.idGenero and
        Libro.Editorial_idEditorial = Editorial.idEditorial where Autor.idAutor = %(idAutor)s;"""

        self.libreria.run_sql(sql, {"idAutor": self.ids[self.combo.current()]})

        libros(self.libreria, self.ids[self.combo.current()])

class libros:
    def __init__(self, db, libro):
        self.libreria = db
        self.data = []
        self.libro = libro

        # Ventana emergente
        self.new = tk.Toplevel()

        # Funcionalidades
        self.__config_window()
        self.__config_treeview_vista()

        self.llenar_treeview_vista()

    def __config_window(self):
        # Ajustes de ventana
        self.new.geometry('500x300')
        titulo = "Libro-Género-Editorial: " + str(self.libro)
        self.new.title(titulo)
        self.new.resizable(width = 0, height = 0)

    def __config_treeview_vista(self):
        self.treeview = ttk.Treeview(self.new)
        self.treeview.configure(columns = ("#0", "#1", "#2"))
        self.treeview.heading("#0", text = "Título")
        self.treeview.heading("#1", text = "Género")
        self.treeview.heading("#2", text = "Editorial")
        self.treeview.column("#0", minwidth = 300, width = 300, stretch = False)
        self.treeview.column("#1", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#2", minwidth = 100, width = 100, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 300, width = 500)
        self.llenar_treeview_vista()
        self.new.after(0, self.llenar_treeview_vista)

    def llenar_treeview_vista(self):
        row_data = """select Titulo, Genero, Editorial from view_autor;"""

        # Guarda info obtenida tras la consulta
        data = self.libreria.run_select(row_data)

        # Evalúa si el contenido de la tabla en la app es distinto al de la db
        if(data != self.data):
            # Elimina todos los rows del treeview si hay diferencias
            self.treeview.delete(*self.treeview.get_children())

            for i in data:
                # Inserta valores en treeview
                self.treeview.insert("", "end", text = i[0],
                    values = (i[1], i[2]), iid = i[0])

            self.data = data
